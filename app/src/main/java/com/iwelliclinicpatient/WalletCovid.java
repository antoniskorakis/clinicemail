package com.iwelliclinicpatient;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;

public class WalletCovid extends AppCompatActivity {
           static TextView WalletButton;
          TextView text;
          TextView heading_amount;
    String data = "";
    String sexo="";
    String ageo="";
    int languageCode;

    public void languageUI(int languageCode) {
        if (languageCode == 0) {
            text.setText(getResources().getString(R.string.in_order_to_get_a_diagnosis_please_proceed_to_the_paymentgr));
            heading_amount.setText(getResources().getString(R.string.amount_1gr));
        } else {
            text.setText(getResources().getString(R.string.in_order_to_get_a_diagnosis_please_proceed_to_the_payment));
            heading_amount.setText(getResources().getString(R.string.amount_1));
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.wallet_pop_up);
            WalletButton = (TextView)findViewById(R.id.wallet_up_next_button);
            text = (TextView)findViewById(R.id.text);

            SharedPreferences sharedPref = getSharedPreferences("USERDATA", MODE_PRIVATE);
            languageCode = sharedPref.getInt("language", 0);
            languageUI(languageCode);
            WalletButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.userinfo, null));
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
                    RadioGroup sex = (RadioGroup) dialog.findViewById(R.id.radioSex);
                    RadioButton male = (RadioButton) dialog.findViewById(R.id.selectImage);
                    RadioButton female = (RadioButton) dialog.findViewById(R.id.analyseImage);
                    EditText age = (EditText) dialog.findViewById(R.id.captureImage);
                    TextView done = (TextView) dialog.findViewById(R.id.captureImage2);
                    if(languageCode==0){
                        options.setText(getResources().getString(R.string.infogr));
                        male.setText(getResources().getString(R.string.malegr));
                        female.setText(getResources().getString(R.string.femalegr));
                        age.setHint(getResources().getString(R.string.agegr));
                    }else{
                        options.setText(getResources().getString(R.string.info));
                        male.setText(getResources().getString(R.string.male));
                        female.setText(getResources().getString(R.string.female));
                        age.setHint(getResources().getString(R.string.age));
                    }

                    ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
                    CancelAlertBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    done.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(male.isChecked())
                            {
                                sexo="male";
                            }
                            else
                            {
                                sexo="female";
                            }

                            ageo= age.getText().toString();

                            if(ageo.isEmpty()){
                                if(languageCode==0){
                                    Toast.makeText(getApplicationContext(), "Συμπληρώστε την ηλικία σας", Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(getApplicationContext(), "Enter your age", Toast.LENGTH_SHORT).show();
                                }
                                return;
                            }
                            dialog.dismiss();
                            covid1(v);
                        }
                    });

                }
            });
        }

    void covid1(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid1, null));
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
        TextView done = (TextView) dialog.findViewById(R.id.buttonOK);
        CheckBox check1 = (CheckBox) dialog.findViewById(R.id.checkBox3);
        CheckBox check2 = (CheckBox) dialog.findViewById(R.id.checkBox4);
        CheckBox check3 = (CheckBox) dialog.findViewById(R.id.checkBox5);
        CheckBox check4 = (CheckBox) dialog.findViewById(R.id.checkBox6);
        CheckBox check5 = (CheckBox) dialog.findViewById(R.id.checkBox7);
        CheckBox check6 = (CheckBox) dialog.findViewById(R.id.checkBox8);
        CheckBox check7 = (CheckBox) dialog.findViewById(R.id.checkBox9);
        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check1.isChecked()) { data+="[{\"id\": \"p_16\",\"choice_id\": \"present\"},"; } else { data+="[{\"id\": \"p_16\",\"choice_id\": \"absent\"},"; }
                if(check2.isChecked()) { data+="{\"id\": \"p_17\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"p_17\",\"choice_id\": \"absent\"},"; }
                if(check3.isChecked()) { data+="{\"id\": \"p_18\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"p_18\",\"choice_id\": \"absent\"},"; }
                if(check4.isChecked()) { data+="{\"id\": \"p_19\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"p_19\",\"choice_id\": \"absent\"},"; }
                if(check5.isChecked()) { data+="{\"id\": \"p_20\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"p_20\",\"choice_id\": \"absent\"},"; }
                if(check6.isChecked()) { data+="{\"id\": \"p_21\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"p_21\",\"choice_id\": \"absent\"},"; }
                if(check7.isChecked()) { data+="{\"id\": \"p_22\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"p_22\",\"choice_id\": \"absent\"},"; }
                dialog.dismiss();
                covid2(v);
            }
        });
    }

    void covid2(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid2, null));
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
        CheckBox check1 = (CheckBox) dialog.findViewById(R.id.checkBox3);
        CheckBox check2 = (CheckBox) dialog.findViewById(R.id.checkBox4);
        CheckBox check3 = (CheckBox) dialog.findViewById(R.id.checkBox5);
        TextView done = (TextView) dialog.findViewById(R.id.buttonOK);
        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check1.isChecked()) { data+="{\"id\": \"s_0\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_0\",\"choice_id\": \"absent\"},"; }
                if(check2.isChecked()) { data+="{\"id\": \"s_1\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_1\",\"choice_id\": \"absent\"},"; }
                if(check3.isChecked()) { data+="{\"id\": \"s_2\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_2\",\"choice_id\": \"absent\"},"; }

                if(check1.isChecked()) {
                    covidfever(v);
                }else{
                    covid3(v);
                }
                dialog.dismiss();
            }
        });
    }

    void covidfever(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covidfever, null));
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
        RadioButton check1 = (RadioButton) dialog.findViewById(R.id.checkBox3);
        RadioButton check2 = (RadioButton) dialog.findViewById(R.id.checkBox4);
        RadioButton check3 = (RadioButton) dialog.findViewById(R.id.checkBox5);
        TextView done = (TextView) dialog.findViewById(R.id.buttonOK);
        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check1.isChecked()) { data+="{\"id\": \"s_3\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_3\",\"choice_id\": \"absent\"},"; }
                if(check2.isChecked()) { data+="{\"id\": \"s_4\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_4\",\"choice_id\": \"absent\"},"; }
                if(check3.isChecked()) { data+="{\"id\": \"s_5\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_5\",\"choice_id\": \"absent\"},"; }
                dialog.dismiss();
                covid3(v);
            }
        });
    }

    void covid3(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid3, null));
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
        CheckBox check1 = (CheckBox) dialog.findViewById(R.id.checkBox3);
        CheckBox check2 = (CheckBox) dialog.findViewById(R.id.checkBox4);
        CheckBox check3 = (CheckBox) dialog.findViewById(R.id.checkBox5);
        TextView done = (TextView) dialog.findViewById(R.id.buttonOK);
        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!check1.isChecked() && !check2.isChecked() && !check3.isChecked()) {
                    if(check1.isChecked()) { data+="{\"id\": \"s_12\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_12\",\"choice_id\": \"absent\"},"; }
                    if(check2.isChecked()) { data+="{\"id\": \"s_13\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_13\",\"choice_id\": \"absent\"},"; }
                    if(check3.isChecked()) { data+="{\"id\": \"s_14\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_14\",\"choice_id\": \"absent\"},"; }
                  covid4(v);
                }else{
                    if(check1.isChecked()) { data+="{\"id\": \"s_12\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_12\",\"choice_id\": \"absent\"},"; }
                    if(check2.isChecked()) { data+="{\"id\": \"s_13\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_13\",\"choice_id\": \"absent\"},"; }
                    if(check3.isChecked()) { data+="{\"id\": \"s_14\",\"choice_id\": \"present\"}]"; } else { data+="{\"id\": \"s_14\",\"choice_id\": \"absent\"}]"; }
                getDiagnosis(data,sexo,ageo);
                }
                dialog.dismiss();
            }
        });
    }

    void covid4(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid4, null));
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
        CheckBox check1 = (CheckBox) dialog.findViewById(R.id.checkBox3);
        CheckBox check2 = (CheckBox) dialog.findViewById(R.id.checkBox4);
        CheckBox check3 = (CheckBox) dialog.findViewById(R.id.checkBox5);
        CheckBox check4 = (CheckBox) dialog.findViewById(R.id.checkBox6);
        CheckBox check5 = (CheckBox) dialog.findViewById(R.id.checkBox7);
        CheckBox check6 = (CheckBox) dialog.findViewById(R.id.checkBox8);
        CheckBox check7 = (CheckBox) dialog.findViewById(R.id.checkBox9);
        TextView done = (TextView) dialog.findViewById(R.id.buttonOK);
        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check1.isChecked()) { data+="{\"id\": \"s_15\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_15\",\"choice_id\": \"absent\"},"; }
                if(check2.isChecked()) { data+="{\"id\": \"s_16\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_16\",\"choice_id\": \"absent\"},"; }
                if(check3.isChecked()) { data+="{\"id\": \"s_17\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_17\",\"choice_id\": \"absent\"},"; }
                if(check4.isChecked()) { data+="{\"id\": \"s_18\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_18\",\"choice_id\": \"absent\"},"; }
                if(check5.isChecked()) { data+="{\"id\": \"s_19\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_19\",\"choice_id\": \"absent\"},"; }
                if(check6.isChecked()) { data+="{\"id\": \"s_20\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_20\",\"choice_id\": \"absent\"},"; }
                if(check7.isChecked()) { data+="{\"id\": \"s_21\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"s_21\",\"choice_id\": \"absent\"},"; }
                covid5(v);
                dialog.dismiss();
            }
        });
    }

    void covid5(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid5, null));
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
        RadioButton check1 = (RadioButton) dialog.findViewById(R.id.checkBox3);
        RadioButton check2 = (RadioButton) dialog.findViewById(R.id.checkBox4);
        RadioButton check3 = (RadioButton) dialog.findViewById(R.id.checkBox5);
        RadioButton check4 = (RadioButton) dialog.findViewById(R.id.checkBox6);
        RadioButton check5 = (RadioButton) dialog.findViewById(R.id.checkBox7);
        TextView done = (TextView) dialog.findViewById(R.id.buttonOK);
        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check1.isChecked()) { data+="{\"id\": \"p_12\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"p_12\",\"choice_id\": \"absent\"},"; }
                if(check2.isChecked()) { data+="{\"id\": \"p_13\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"p_13\",\"choice_id\": \"absent\"},"; }
                if(check3.isChecked()) { data+="{\"id\": \"p_14\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"p_14\",\"choice_id\": \"absent\"},"; }
                if(check4.isChecked()) { data+="{\"id\": \"p_11\",\"choice_id\": \"present\"},"; } else { data+="{\"id\": \"p_11\",\"choice_id\": \"absent\"},"; }
                if(check5.isChecked()) { data+="{\"id\": \"p_15\",\"choice_id\": \"present\"}]"; } else { data+="{\"id\": \"p_15\",\"choice_id\": \"absent\"}]"; }
                getDiagnosis(data,sexo,ageo);
                dialog.dismiss();
            }
        });
    }

    private void getDiagnosis(String Data,String sex,String age) {
      Log.d("DataToSent",data);
        HttpURLConnection connection = null;
        URL url;
        try {
            url = new URL("https://api.infermedica.com/covid19/triage");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            connection.setRequestProperty("App-Id", "06ecefd2");
            connection.setRequestProperty("App-Key", "7f6f98ab70709a5b87111b5bc791e9ff");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            String dataToSend = "{\"sex\": \""+sex+"\",\"age\": "+age+",\"evidence\":"+Data+"}";
            Log.d("toSend",dataToSend);

            byte[] bytes = dataToSend.getBytes("UTF-8");
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.write(bytes);
            wr.flush();
            wr.close();
            if (connection.getResponseMessage().equals("OK")) {
                InputStream in = new BufferedInputStream(connection.getInputStream());
                ByteArrayOutputStream buf = new ByteArrayOutputStream();
                int result = in.read();
                while (result != -1) {
                    buf.write((byte) result);
                    result = in.read();
                }
                String response = buf.toString("UTF-8");

                JSONObject reader = new JSONObject(response);
                String diagnosis="";

                if(!reader.getString("description").equals("null")){
                    diagnosis=reader.getString("description");
                }else{
                    if(languageCode==0){
                        diagnosis = "Δεν έχετε μολυνθεί από τον COVID-19";
                    }else {
                        diagnosis = "You have not been infected with COVID-19";
                    }
                }
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                builder.setMessage(diagnosis)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                finish();
                            }
                        });
                android.app.AlertDialog alert = builder.create();
                alert.show();
            } else {
                Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        } catch (UnknownHostException | JSONException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (ProtocolException e) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (ConnectException e) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (MalformedURLException e) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

}
