package com.iwelliclinicpatient;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;

public class symptomsActivity extends AppCompatActivity {

    protected Context mContext;
    int languageCode = 0;

    CardView head_skull_card_view;
    CardView head_skull_drop_down_card_view;
    ImageView head_skull_image_right;
    CardView chest_card_view;
    CardView chest_drop_down_card_view;
    ImageView  chest_image_right;
    CardView belly_card_view;
    CardView  belly_drop_down_card_view;
    ImageView  belly_image_right;
    CardView breast_card_view;
    CardView breast_drop_down_card_view;
    ImageView breast_image_right;
    CardView psychological_card_view;
    CardView psychological_drop_down_card_view;
    ImageView  psychological_image_right;
    CardView  general_card_view;
    CardView  general_drop_down_card_view;
    ImageView  general_image_right;
    CardView  respiratory_card_view;
    CardView  respiratory_drop_down_card_view;
    ImageView  respiratory_image_right;
    CardView cardiovascular_card_view;
    CardView  cardiovascular_drop_down_card_view;
    ImageView  cardiovascular_image_right;
    CardView  lower_limbs_card_view;
    CardView  lower_limbs_drop_down_card_view;
    ImageView  lower_limbs_image_right;
    CardView musculoskeletal_system_card_view;
    CardView  musculoskeletal_system_drop_down_card_view;
    ImageView  musculoskeletal_system_image_right;
    TextView symptom_done_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        SharedPreferences sharedPref = getSharedPreferences("USERDATA", MODE_PRIVATE);
        languageCode = sharedPref.getInt("language", 0);
        if(languageCode==0){
            setContentView(R.layout.symptoms_drop_downgr);
        }else{
            setContentView(R.layout.symptoms_drop_down);
        }
        symptom_done_button = (TextView)findViewById(R.id.symptom_done_button);
        head_skull_card_view=findViewById(R.id.head_skull_card_view);
        head_skull_drop_down_card_view=findViewById(R.id.head_skull_drop_down_card_view);
        head_skull_image_right=findViewById(R.id.head_skull_image_right);
        chest_card_view=findViewById(R.id.chest_card_view);
        chest_drop_down_card_view=findViewById(R.id.chest_drop_down_card_view);
        chest_image_right=findViewById(R.id.chest_image_right);
        belly_card_view=findViewById(R.id.belly_card_view);
        belly_drop_down_card_view=findViewById(R.id.belly_drop_down_card_view);
        belly_image_right=findViewById(R.id.belly_image_right);
        breast_card_view=findViewById(R.id.breast_card_view);
        breast_drop_down_card_view=findViewById(R.id.breast_drop_down_card_view);
        breast_image_right=findViewById(R.id.breast_image_right);
        psychological_card_view=findViewById(R.id.psychological_card_view);
        psychological_drop_down_card_view=findViewById(R.id.psychological_drop_down_card_view);
        psychological_image_right=findViewById(R.id.psychological_image_right);
        general_card_view=findViewById(R.id.general_card_view);
        general_drop_down_card_view=findViewById(R.id.general_drop_down_card_view);
        general_image_right=findViewById(R.id.general_image_right);
        respiratory_card_view=findViewById(R.id.respiratory_card_view);
        respiratory_drop_down_card_view=findViewById(R.id.respiratory_drop_down_card_view);
        respiratory_image_right=findViewById(R.id.respiratory_image_right);
        cardiovascular_card_view=findViewById(R.id.cardiovascular_card_view);
        cardiovascular_drop_down_card_view=findViewById(R.id.cardiovascular_drop_down_card_view);
        cardiovascular_image_right=findViewById(R.id.cardiovascular_image_right);
        lower_limbs_card_view=findViewById(R.id.lower_limbs_card_view);
        lower_limbs_drop_down_card_view=findViewById(R.id.lower_limbs_drop_down_card_view);
        lower_limbs_image_right=findViewById(R.id.lower_limbs_image_right);
        musculoskeletal_system_card_view=findViewById(R.id.musculoskeletal_system_card_view);
        musculoskeletal_system_drop_down_card_view=findViewById(R.id.musculoskeletal_system_drop_down_card_view);
        musculoskeletal_system_image_right=findViewById(R.id.musculoskeletal_system_image_right);

        mContext = this;

        head_skull_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(head_skull_drop_down_card_view.isShown()) {
                    head_skull_drop_down_card_view.setVisibility(View.GONE);
                    head_skull_image_right.animate().rotation(360.00f).start();
                }else {
                    head_skull_drop_down_card_view.setVisibility(View.VISIBLE);
                    head_skull_image_right.animate().rotation(270).start();
                }
            }
        });

        symptom_done_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox)findViewById(R.id.head_skull_checkbox_1)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.head_skull_checkbox_1)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.head_skull_checkbox_2)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.head_skull_checkbox_2)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.head_skull_checkbox_3)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.head_skull_checkbox_3)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.head_skull_checkbox_4)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.head_skull_checkbox_4)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.head_skull_checkbox_5)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.head_skull_checkbox_5)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.head_skull_checkbox_6)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.head_skull_checkbox_6)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.head_skull_checkbox_7)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.head_skull_checkbox_7)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.head_skull_checkbox_8)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.head_skull_checkbox_8)).getText()+",";
                }

                if (((CheckBox)findViewById(R.id.chest_checkbox_1)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.chest_checkbox_1)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.chest_checkbox_2)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.chest_checkbox_2)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.chest_checkbox_3)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.chest_checkbox_3)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.chest_checkbox_4)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.chest_checkbox_4)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.chest_checkbox_5)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.chest_checkbox_5)).getText()+",";
                }

                if (((CheckBox)findViewById(R.id.belly_checkbox_1)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.belly_checkbox_1)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.belly_checkbox_2)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.belly_checkbox_2)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.belly_checkbox_3)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.belly_checkbox_3)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.belly_checkbox_4)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.belly_checkbox_4)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.belly_checkbox_5)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.belly_checkbox_5)).getText()+",";
                }

                if (((CheckBox)findViewById(R.id.breast_checkbox_1)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.breast_checkbox_1)).getText()+",";
                }

                if (((CheckBox)findViewById(R.id.psychological_checkbox_1)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.psychological_checkbox_1)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.psychological_checkbox_2)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.psychological_checkbox_2)).getText()+",";
                }

                if (((CheckBox)findViewById(R.id.general_checkbox_1)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.general_checkbox_1)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.general_checkbox_4)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.general_checkbox_4)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.general_checkbox_5)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.general_checkbox_5)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.general_checkbox_7)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.general_checkbox_7)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.general_checkbox_8)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.general_checkbox_8)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.general_checkbox_9)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.general_checkbox_9)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.general_checkbox_10)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.general_checkbox_10)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.general_checkbox_11)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.general_checkbox_11)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.general_checkbox_12)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.general_checkbox_12)).getText()+",";
                }

                if (((CheckBox)findViewById(R.id.respiratory_checkbox_1)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.respiratory_checkbox_1)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.respiratory_checkbox_2)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.respiratory_checkbox_2)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.respiratory_checkbox_3)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.respiratory_checkbox_3)).getText()+",";
                }

                if (((CheckBox)findViewById(R.id.cardiovascular_checkbox_1)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.cardiovascular_checkbox_1)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.cardiovascular_checkbox_2)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.cardiovascular_checkbox_2)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.cardiovascular_checkbox_3)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.cardiovascular_checkbox_3)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.cardiovascular_checkbox_4)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.cardiovascular_checkbox_4)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.cardiovascular_checkbox_5)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.cardiovascular_checkbox_5)).getText()+",";
                }

                if (((CheckBox)findViewById(R.id.lower_limbs_checkbox_1)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.lower_limbs_checkbox_1)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.lower_limbs_checkbox_2)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.lower_limbs_checkbox_2)).getText()+",";
                }

                if (((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_1)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_1)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_2)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_2)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_3)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_3)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_4)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_4)).getText()+",";
                }
                if (((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_5)).isChecked()) {
                    Home.EmailSymptoms+=((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_5)).getText()+",";
                }
                if(Home.EmailSymptoms!="") {
                    Home.SendEmail = true;
                    Home.SymptomsImage.setImageResource(R.drawable.ic_symptommark1);
                    if(languageCode==0){
                        Home.SymptomsButton.setText("ΠΡΟΣΘΗΚΗ ΣΥΜΠΤΩΜΑΤΩΝ ΚΑΙ ΔΙΑΓΝΩΣΗ");
                    }else {
                        Home.SymptomsButton.setText("ADD SYMPTOMS OR GET DIAGNOSIS");
                    }
                }
                finish();
            }
        });

        chest_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chest_drop_down_card_view.isShown()) {
                    chest_drop_down_card_view.setVisibility(View.GONE);
                    chest_image_right.animate().rotation(360.00f).start();
                }else {
                    chest_drop_down_card_view.setVisibility(View.VISIBLE);
                    chest_image_right.animate().rotation(270).start();
                }
            }
        });

        belly_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(belly_drop_down_card_view.isShown()) {
                    belly_drop_down_card_view.setVisibility(View.GONE);
                    belly_image_right.animate().rotation(360.00f).start();
                }else {
                    belly_drop_down_card_view.setVisibility(View.VISIBLE);
                    belly_image_right.animate().rotation(270).start();
                }
            }
        });

        breast_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(breast_drop_down_card_view.isShown()) {
                    breast_drop_down_card_view.setVisibility(View.GONE);
                    breast_image_right.animate().rotation(360.00f).start();
                }else {
                    breast_drop_down_card_view.setVisibility(View.VISIBLE);
                    breast_image_right.animate().rotation(270).start();
                }
            }
        });

        psychological_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(psychological_drop_down_card_view.isShown()) {
                    psychological_drop_down_card_view.setVisibility(View.GONE);
                    psychological_image_right.animate().rotation(360.00f).start();
                }else {
                    psychological_drop_down_card_view.setVisibility(View.VISIBLE);
                    psychological_image_right.animate().rotation(270).start();
                }
            }
        });

        general_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(general_drop_down_card_view.isShown()) {
                    general_drop_down_card_view.setVisibility(View.GONE);
                    general_image_right.animate().rotation(360.00f).start();
                }else {
                    general_drop_down_card_view.setVisibility(View.VISIBLE);
                    general_image_right.animate().rotation(270).start();
                }
            }
        });

        respiratory_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(respiratory_drop_down_card_view.isShown()) {
                    respiratory_drop_down_card_view.setVisibility(View.GONE);
                    respiratory_image_right.animate().rotation(360.00f).start();
                }else {
                    respiratory_drop_down_card_view.setVisibility(View.VISIBLE);
                    respiratory_image_right.animate().rotation(270).start();
                }
            }
        });

        cardiovascular_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cardiovascular_drop_down_card_view.isShown()) {
                    cardiovascular_drop_down_card_view.setVisibility(View.GONE);
                    cardiovascular_image_right.animate().rotation(360.00f).start();
                }else {
                    cardiovascular_drop_down_card_view.setVisibility(View.VISIBLE);
                    cardiovascular_image_right.animate().rotation(270).start();
                }
            }
        });

        lower_limbs_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lower_limbs_drop_down_card_view.isShown()) {
                    lower_limbs_drop_down_card_view.setVisibility(View.GONE);
                    lower_limbs_image_right.animate().rotation(360.00f).start();
                }else {
                    lower_limbs_drop_down_card_view.setVisibility(View.VISIBLE);
                    lower_limbs_image_right.animate().rotation(270).start();
                }
            }
        });

        musculoskeletal_system_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(musculoskeletal_system_drop_down_card_view.isShown()) {
                    musculoskeletal_system_drop_down_card_view.setVisibility(View.GONE);
                    musculoskeletal_system_image_right.animate().rotation(360.00f).start();
                }else {
                    musculoskeletal_system_drop_down_card_view.setVisibility(View.VISIBLE);
                    musculoskeletal_system_image_right.animate().rotation(270).start();
                }
            }
        });

        String[] splited = Home.EmailSymptoms.split(",");

        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.head_skull_checkbox_1)).getText())) {
            ((CheckBox)findViewById(R.id.head_skull_checkbox_1)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.head_skull_checkbox_2)).getText())) {
            ((CheckBox)findViewById(R.id.head_skull_checkbox_2)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.head_skull_checkbox_3)).getText())) {
            ((CheckBox)findViewById(R.id.head_skull_checkbox_3)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.head_skull_checkbox_4)).getText())) {
            ((CheckBox)findViewById(R.id.head_skull_checkbox_4)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.head_skull_checkbox_5)).getText())) {
            ((CheckBox)findViewById(R.id.head_skull_checkbox_5)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.head_skull_checkbox_6)).getText())) {
            ((CheckBox)findViewById(R.id.head_skull_checkbox_6)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.head_skull_checkbox_7)).getText())) {
            ((CheckBox)findViewById(R.id.head_skull_checkbox_7)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.head_skull_checkbox_8)).getText())) {
            ((CheckBox)findViewById(R.id.head_skull_checkbox_8)).setChecked(true);
        }

        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.chest_checkbox_1)).getText())) {
            ((CheckBox)findViewById(R.id.chest_checkbox_1)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.chest_checkbox_2)).getText())) {
            ((CheckBox)findViewById(R.id.chest_checkbox_2)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.chest_checkbox_3)).getText())) {
            ((CheckBox)findViewById(R.id.chest_checkbox_3)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.chest_checkbox_4)).getText())) {
            ((CheckBox)findViewById(R.id.chest_checkbox_4)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.chest_checkbox_5)).getText())) {
            ((CheckBox)findViewById(R.id.chest_checkbox_5)).setChecked(true);
        }

        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.belly_checkbox_1)).getText())) {
            ((CheckBox)findViewById(R.id.belly_checkbox_1)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.belly_checkbox_2)).getText())) {
            ((CheckBox)findViewById(R.id.belly_checkbox_2)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.belly_checkbox_3)).getText())) {
            ((CheckBox)findViewById(R.id.belly_checkbox_3)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.belly_checkbox_4)).getText())) {
            ((CheckBox)findViewById(R.id.belly_checkbox_4)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.belly_checkbox_5)).getText())) {
            ((CheckBox)findViewById(R.id.belly_checkbox_5)).setChecked(true);
        }

        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.breast_checkbox_1)).getText())) {
            ((CheckBox)findViewById(R.id.breast_checkbox_1)).setChecked(true);
        }

        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.psychological_checkbox_1)).getText())) {
            ((CheckBox)findViewById(R.id.psychological_checkbox_1)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.psychological_checkbox_2)).getText())) {
            ((CheckBox)findViewById(R.id.psychological_checkbox_2)).setChecked(true);
        }

        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.general_checkbox_1)).getText())) {
            ((CheckBox)findViewById(R.id.general_checkbox_1)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.general_checkbox_4)).getText())) {
            ((CheckBox)findViewById(R.id.general_checkbox_4)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.general_checkbox_5)).getText())) {
            ((CheckBox)findViewById(R.id.general_checkbox_5)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.general_checkbox_7)).getText())) {
            ((CheckBox)findViewById(R.id.general_checkbox_7)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.general_checkbox_8)).getText())) {
            ((CheckBox)findViewById(R.id.general_checkbox_8)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.general_checkbox_9)).getText())) {
            ((CheckBox)findViewById(R.id.general_checkbox_9)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.general_checkbox_10)).getText())) {
            ((CheckBox)findViewById(R.id.general_checkbox_10)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.general_checkbox_11)).getText())) {
            ((CheckBox)findViewById(R.id.general_checkbox_11)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.general_checkbox_12)).getText())) {
            ((CheckBox)findViewById(R.id.general_checkbox_12)).setChecked(true);
        }

        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.respiratory_checkbox_1)).getText())) {
            ((CheckBox)findViewById(R.id.respiratory_checkbox_1)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.respiratory_checkbox_2)).getText())) {
            ((CheckBox)findViewById(R.id.respiratory_checkbox_2)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.respiratory_checkbox_3)).getText())) {
            ((CheckBox)findViewById(R.id.respiratory_checkbox_3)).setChecked(true);
        }

        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.cardiovascular_checkbox_1)).getText())) {
            ((CheckBox)findViewById(R.id.cardiovascular_checkbox_1)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.cardiovascular_checkbox_2)).getText())) {
            ((CheckBox)findViewById(R.id.cardiovascular_checkbox_2)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.cardiovascular_checkbox_3)).getText())) {
            ((CheckBox)findViewById(R.id.cardiovascular_checkbox_3)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.cardiovascular_checkbox_4)).getText())) {
            ((CheckBox)findViewById(R.id.cardiovascular_checkbox_4)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.cardiovascular_checkbox_5)).getText())) {
            ((CheckBox)findViewById(R.id.cardiovascular_checkbox_5)).setChecked(true);
        }

        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.lower_limbs_checkbox_1)).getText())) {
            ((CheckBox)findViewById(R.id.lower_limbs_checkbox_1)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.lower_limbs_checkbox_2)).getText())) {
            ((CheckBox)findViewById(R.id.lower_limbs_checkbox_2)).setChecked(true);
        }

        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_1)).getText())) {
            ((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_1)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_2)).getText())) {
            ((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_2)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_3)).getText())) {
            ((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_3)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_4)).getText())) {
            ((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_4)).setChecked(true);
        }
        if (Arrays.asList(splited).contains(((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_5)).getText())) {
            ((CheckBox)findViewById(R.id.musculoskeletal_system_checkbox_5)).setChecked(true);
        }

    }
}
