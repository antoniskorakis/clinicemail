package com.iwelliclinicpatient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

public class LoginActivity extends Activity {
    String patientName = "";
    String doctorEmail = "";
    String doctorPhone = "";
    TextView textView;
    TextView login;
    EditText username;
    EditText password;
    EditText telephone;
    Spinner languageSelect;
    int languageCode = 0;
    Boolean first = true;
    private ImageView toolbarbackgroundcolor;
    private ImageView backbutton;
    private ConstraintLayout circleimagelayout;
    private TextView toolbartext;
    private ConstraintLayout righticonslayout;
    TextView tv_heading_language;

    public void languageUI(int languageCode) {
        if (languageCode == 0) {
            textView.setText(getResources().getString(R.string.titleGR));
            login.setText(getResources().getString(R.string.loginGR));
            password.setHint(getResources().getString(R.string.DoctorEmailGR));
            username.setHint(getResources().getString(R.string.PatientNameGR));
            telephone.setHint(getResources().getString(R.string.telephonegr));
            toolbartext.setText(getResources().getString(R.string.loginGR));
            String[] items = new String[]{"Ελληνικά", "Αγγλικά"};
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
            languageSelect.setAdapter(adapter);
            languageSelect.setSelection(0);
            tv_heading_language.setText("Γλώσσα");
        } else {
            login.setText(getResources().getString(R.string.login));
            password.setHint(getResources().getString(R.string.DoctorEmail));
            username.setHint(getResources().getString(R.string.PatientName));
            telephone.setHint(getResources().getString(R.string.telephone));
            textView.setText(getResources().getString(R.string.titleEN));
            String[] items = new String[]{"Greek", "English"};
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
            languageSelect.setAdapter(adapter);
            languageSelect.setSelection(1);
            toolbartext.setText(getResources().getString(R.string.login));
            tv_heading_language.setText("Language");
        }
    }

    private void setToolBar(){
        toolbarbackgroundcolor.setVisibility(textView.GONE);
        backbutton.setVisibility(textView.GONE);
        circleimagelayout.setVisibility(textView.GONE);
        righticonslayout.setVisibility(textView.GONE);
        toolbartext.setTextColor(getResources().getColor(R.color.button_color));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPref = getSharedPreferences("USERDATA", MODE_PRIVATE);
        languageCode = sharedPref.getInt("language", 0);
        patientName=sharedPref.getString("patientName", "");
        doctorEmail=sharedPref.getString("doctorEmail", "");
        doctorPhone=sharedPref.getString("doctorPhone", "");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.login_activity);

        righticonslayout=findViewById(R.id.right_icons_layout);
        toolbarbackgroundcolor=findViewById(R.id.toolbar_background_color);
        backbutton=findViewById(R.id.back_button);
        circleimagelayout=findViewById(R.id.circle_image_layout);
        toolbartext=findViewById(R.id.toolbar_text);
        languageSelect = (Spinner) findViewById(R.id.spinner_drop_down_language);
        textView = (TextView) findViewById(R.id.tv_image_logo_text);
        username =(EditText) findViewById(R.id.et_username);
        password =(EditText)findViewById(R.id.et_password);
        telephone =(EditText)findViewById(R.id.et_telephone);
        login = (TextView) findViewById(R.id.tv_login);
        tv_heading_language=(TextView)findViewById(R.id.tv_heading_language);
        languageUI(languageCode);
        username.setText(patientName);
        password.setText(doctorEmail);
        telephone.setText(doctorPhone);

        languageSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (!first) {
                    SharedPreferences.Editor editor = getSharedPreferences("USERDATA", MODE_PRIVATE).edit();
                    editor.putInt("language", position);
                    editor.apply();
                    languageCode = position;
                    languageUI(languageCode);
                }
                first = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                patientName=username.getText().toString();
                doctorEmail=password.getText().toString();
                doctorPhone=telephone.getText().toString();

                if((!TextUtils.isEmpty(doctorEmail) && Patterns.EMAIL_ADDRESS.matcher(doctorEmail).matches())) {
                    if (!TextUtils.isEmpty(patientName) || !TextUtils.isEmpty(doctorEmail)) {
                        SharedPreferences.Editor editor = getBaseContext().getSharedPreferences("USERDATA", MODE_PRIVATE).edit();
                        editor.putString("patientName", patientName);
                        editor.putString("doctorEmail", doctorEmail);
                        editor.putString("doctorPhone", doctorPhone);
                        editor.apply();
                        Intent intent = new Intent(LoginActivity.this, BasicMenu.class);
                        startActivity(intent);
                        finish();
                    }
                }else{
                    if(languageCode==0) {
                        showToast(getApplicationContext(), getResources().getString(R.string.emailError));
                    }else{
                        showToast(getApplicationContext(), getResources().getString(R.string.emailErrorGR));
                    }
                }
            }
        });
        setToolBar();
    }
    public static void showToast(Context mContext, String message){
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }
}