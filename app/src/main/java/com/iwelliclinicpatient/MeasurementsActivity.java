package com.iwelliclinicpatient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class MeasurementsActivity extends AppCompatActivity {
    private static Activity activity;
    ArrayAdapter adapter;
    RecyclerView listView;
    Bundle bundle = null;
    private static String session;
    private static String doctorID;
    private static String doctorEmail;
    private static Boolean enableBox;
    ArrayList<Patient> patients = new ArrayList();
    private static Context mContext;
    Button logoutButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurements);
        Toolbar toolbar = findViewById(R.id.toolbar);
        logoutButton = (Button) findViewById(R.id.logoutButton);
        SharedPreferences sharedPref = getSharedPreferences("USERDATA", MODE_PRIVATE);
        activity = this;
        mContext = this;

        /*
        if ((sharedPref.getInt("language", 0)) == 0) {
            logoutButton.setText(getResources().getString(R.string.BoxLogoutGR));
            toolbar.setTitle(getResources().getString(R.string.DoctorsPatientsGR));
        }else{
            logoutButton.setText(getResources().getString(R.string.BoxLogoutEN));
            toolbar.setTitle(getResources().getString(R.string.DoctorsPatientsEN));
        }
        setSupportActionBar(toolbar);
        */
        bundle = getIntent().getExtras();
        session = bundle.getString("session");
        doctorEmail = bundle.getString("doctorEmail");
        enableBox = bundle.getBoolean("enableBox");
        listView = (RecyclerView) findViewById(R.id.availablePatients);
        /*
        logoutButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CreateMeasurement.sessionBox != null) {
                    CreateMeasurement.sessionBox.logout();
                }
                SharedPreferences.Editor editor = getSharedPreferences("USERDATA", MODE_PRIVATE).edit();
                editor.remove("session");
                editor.remove("doctorID");
                editor.remove("enableBox");
                editor.remove("SelectedPatient");
                editor.remove("doctorEmail");
                editor.apply();
                finishAffinity();
            }
        });
*/
        GetAllPatients();
    }

    public static void selectPatient(String patientID){
        SharedPreferences.Editor editor = mContext.getSharedPreferences("USERDATA", MODE_PRIVATE).edit();
        editor.putString("session", session);
     //   editor.putString("doctorID", doctorID);
        editor.putString("doctorEmail", doctorEmail);
        editor.putBoolean("enableBox", enableBox);
        editor.putString("SelectedPatient", patientID);

        editor.apply();
        Intent intent = new Intent(mContext, BasicMenu.class);
        mContext.startActivity(intent);
        activity.finish();
    }

    private void GetAllPatients() {
        HttpURLConnection connection = null;
        URL url;
        Date now = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String nowAsISO = dateFormat.format(now);
        try {
            url = new URL("https://healthcare.googleapis.com/v1beta1/projects/sylvan-harmony-255415/locations/europe-west2/datasets/Clinic1/fhirStores/"+doctorEmail.replace("@","")+"/fhir/Patient");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "Bearer " + session);
            connection.setRequestProperty("cache-control", "no-cache");
            connection.setUseCaches(false);
            connection.setDoInput(true);

            Log.d("LogTest",session);
            Log.d("LogTest",url.toString());
            Log.d("LogTest",connection.getResponseMessage());


            if (connection.getResponseMessage().equals("OK")) {
                InputStream in = new BufferedInputStream(connection.getInputStream());
                ByteArrayOutputStream buf = new ByteArrayOutputStream();
                int result = in.read();
                while (result != -1) {
                    buf.write((byte) result);
                    result = in.read();
                }
                String response = buf.toString("UTF-8");
                JSONObject reader = new JSONObject(response);
                int count = reader.getInt("total");
                JSONArray patientArray = reader.getJSONArray("entry");

                for (int i = 0; i < count; i++) {
                    JSONObject obj = patientArray.getJSONObject(i);
                    JSONObject resource = obj.getJSONObject("resource");
                    JSONArray nameArray = resource.getJSONArray("name");
                    JSONObject nameobj = nameArray.getJSONObject(0);
                    JSONArray givenArray = nameobj.getJSONArray("given");

                    patients.add(new Patient(resource.getString("id"),nameobj.getString("family")+ " "+ givenArray.getString(0)));
                }
                PatientAdapter adapter = new PatientAdapter(patients);
                listView.setAdapter(adapter);
                listView.setLayoutManager(new LinearLayoutManager(this));


            } else {
                Toast.makeText(getApplicationContext(), "Please Login again!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MeasurementsActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        } catch (UnknownHostException e) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (ProtocolException e) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (ConnectException e) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (MalformedURLException e) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}

