package com.iwelliclinicpatient;

import android.Manifest;
import android.app.DownloadManager;
import android.bluetooth.BluetoothAdapter;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.borsam.network.BorsamHttp;
import com.borsam.network.BorsamRequest;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.iwelliclinicpatient.bean.BorsamResult;
import com.iwelliclinicpatient.bean.Config;
import com.iwelliclinicpatient.bean.LoginResult;
import com.iwelliclinicpatient.bean.UploadResult;
import com.iwelliclinicpatient.util.HttpUtil;
import com.iwelliclinicpatient.util.MD5Utils;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.DOWNLOAD_SERVICE;
import static android.content.Context.MODE_PRIVATE;

public class Home extends Fragment {
    static TextView ECGButton;
    static TextView VitalsButton;
    static TextView SymptomsButton;

    ArrayList<Uri> uris;
    TextView tv_ecg_heading;
    TextView ecg_text;
    TextView tv_view_ecg;
    TextView tv_symptoms_heading;
    TextView symptoms_text;
    TextView tv_start_symptoms;
    TextView tv_vital_notes_heading;
    TextView vital_notes_text;
    TextView tv_view_vital_notes;
    TextView tv_notes_heading;
    TextView tv_send_data;
    static String ECGcomments = "";
    static int ECGType = 0;
    static int ECGTime = 0;
    static int IschemiaScreenNumber = 0;
    private static String doctorEmail;
    private static String patientName;
    private static String doctorPhone;
    static Boolean SendEmail = false;
    public static int languageCode = 0;
    TextView commentsText;
    static Boolean ECGAnalysis = false;
    static Boolean ECGExists = false;
    static ImageView ECGImage;
    static ImageView VitalsImage;
    static ImageView SymptomsImage;
    static ImageView PhotoImage;
    private ImageView toolbarbackgroundcolor;
    private ImageView backbutton;
    private ConstraintLayout circleimagelayout;
    private TextView toolbartext;
    private ConstraintLayout righticonslayout;
    static String EmailVital = "";
    static String EmailSymptoms = "";
    static String EmailDiagnosis = "";
    static String bloodPressure1 = "";
    static String bloodPressure2 = "";
    static String oxygen = "";
    static String weight = "";
    static String heartrate = "";
    static String gluco = "";
    static String temp = "";
    String selectedImage = "";

    String data = "";
    String sexo="";
    String ageo="";
    String locationSend="";

    private FusedLocationProviderClient fusedLocationClient;
    public static final int RESULT_LOAD_IMAGE = 11234;
    public static final int CAMERA_REQUEST = 132;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    long downloadID;

    View view;

    public void bluetoothCheck() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 1234);
        }
        locationCheck();
    }

    public boolean locationCheck() {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            String ques;
            if (languageCode == 0) {
                ques="Πρέπει να ενεργοποιήσετε το GPS";
            }
            else{
                ques="You need to enable your GPS";
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(ques)
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),0001);
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
        fusedLocationClient =  LocationServices.getFusedLocationProviderClient(getActivity());
        getLocation();
        return true;
    }

    public void languageUI(int languageCode) {
        if (languageCode == 0) {
            tv_ecg_heading.setText(getResources().getString(R.string.tv_ecg_headinggr));
            ecg_text.setText(getResources().getString(R.string.ecg_textgr));
            tv_view_ecg.setText(getResources().getString(R.string.tv_start_ecggr));
            tv_symptoms_heading.setText(getResources().getString(R.string.tv_symptoms_headinggr));
            symptoms_text.setText(getResources().getString(R.string.symptoms_textgr));
            tv_start_symptoms.setText(getResources().getString(R.string.tv_start_symptomsgr));
            tv_vital_notes_heading.setText(getResources().getString(R.string.tv_vital_notes_headinggr));
            vital_notes_text.setText(getResources().getString(R.string.vital_notes_textgr));
            tv_view_vital_notes.setText(getResources().getString(R.string.tv_start_vital_notesgr));
            tv_notes_heading.setText(getResources().getString(R.string.tv_notes_headinggr));
            tv_send_data.setText(getResources().getString(R.string.tv_send_datagr));

        } else {
            tv_ecg_heading.setText(getResources().getString(R.string.tv_ecg_heading));
            ecg_text.setText(getResources().getString(R.string.ecg_text));
            tv_view_ecg.setText(getResources().getString(R.string.tv_start_ecg));
            tv_symptoms_heading.setText(getResources().getString(R.string.tv_symptoms_heading));
            symptoms_text.setText(getResources().getString(R.string.symptoms_text));
            tv_start_symptoms.setText(getResources().getString(R.string.tv_start_symptoms));
            tv_vital_notes_heading.setText(getResources().getString(R.string.tv_vital_notes_heading));
            vital_notes_text.setText(getResources().getString(R.string.vital_notes_text));
            tv_view_vital_notes.setText(getResources().getString(R.string.tv_start_vital_notes));
            tv_notes_heading.setText(getResources().getString(R.string.tv_notes_heading));
            tv_send_data.setText(getResources().getString(R.string.tv_send_data));
        }
    }

    private boolean isPackageInstalled(String packageName, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }
    }

    public static void showToast(Context mContext, String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    private BroadcastReceiver onDownloadComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            if (downloadID == id) {
                if(languageCode==0){
                    showToast(getContext(), "Η ανάλυση ΗΛΚ τελείωσε");
                    ECGButton.setText("ΔΕΙΤΕ ΑΝΑΛΥΣΗ ΗΛΚ");
                }else {
                    showToast(getContext(), "ECG Analysis Finished");
                    ECGButton.setText("VIEW ECG ANALYSIS");
                }
            }
        }
    };

        private void requestPermissionCamera() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home, container, false);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        getActivity().registerReceiver(onDownloadComplete,new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        tv_ecg_heading=view.findViewById(R.id.tv_ecg_heading);
        ecg_text=view.findViewById(R.id.ecg_text);
        tv_view_ecg=view.findViewById(R.id.tv_start_ecg);
        tv_symptoms_heading=view.findViewById(R.id.tv_symptoms_heading);
        symptoms_text=view.findViewById(R.id.symptoms_text);
        tv_start_symptoms=view.findViewById(R.id.tv_start_symptoms);
        tv_vital_notes_heading=view.findViewById(R.id.tv_vital_notes_heading);
        vital_notes_text=view.findViewById(R.id.vital_notes_text);
        tv_view_vital_notes=view.findViewById(R.id.tv_start_vital_notes);
        tv_notes_heading=view.findViewById(R.id.tv_notes_heading);
        tv_send_data=view.findViewById(R.id.tv_send_data);
        bluetoothCheck();
        ECGButton = (TextView) view.findViewById(R.id.tv_start_ecg);
        VitalsButton = (TextView) view.findViewById(R.id.tv_start_vital_notes);
        SymptomsButton = (TextView) view.findViewById(R.id.tv_start_symptoms);
        commentsText = (TextView) view.findViewById(R.id.comment_text);
        PhotoImage = (ImageView) view.findViewById(R.id.notes_test_image);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        SharedPreferences sharedPref = this.getActivity().getSharedPreferences("USERDATA", MODE_PRIVATE);
        patientName = sharedPref.getString("patientName", "");
        doctorEmail = sharedPref.getString("doctorEmail", "");
        languageCode = sharedPref.getInt("language", 0);
        doctorPhone= sharedPref.getString("doctorPhone", "");
        TextView SendData = (TextView) view.findViewById(R.id.tv_send_data);
        ECGImage = (ImageView) view.findViewById(R.id.ecg_test_image);
        VitalsImage = (ImageView) view.findViewById(R.id.vital_notes_test_image);
        SymptomsImage = (ImageView) view.findViewById(R.id.symptoms_test_image);
        righticonslayout = getActivity().findViewById(R.id.right_icons_layout);
        toolbarbackgroundcolor = getActivity().findViewById(R.id.toolbar_background_color);
        backbutton = getActivity().findViewById(R.id.back_button);
        circleimagelayout = getActivity().findViewById(R.id.circle_image_layout);
        toolbartext = getActivity().findViewById(R.id.toolbar_text);

        languageUI(languageCode);
        Button fab = view.findViewById(R.id.mic_select);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startVoiceInput();
            }
        });

        requestPermissionCamera();

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }
        } else {
            Log.d("Directory", getActivity().getExternalFilesDir(null) + "/files/");
        }


        FloatingActionButton fab2 = view.findViewById(R.id.VideoCall);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PackageManager pm = getContext().getPackageManager();
                if(isPackageInstalled("com.google.android.apps.tachyon",pm)) {
                    Intent intent = new Intent();
                    intent.setPackage("com.google.android.apps.tachyon");
                    intent.setAction("com.google.android.apps.tachyon.action.CALL");
                    intent.setData(Uri.parse("tel:"+doctorPhone));
                    startActivity(intent);
                }else{
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.tachyon")));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.tachyon")));
                    }
                }
            }
        });

        SendData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SendEmail) {
                    if ((!SymptomsButton.getText().toString().equals("ADD SYMPTOMS OR GET DIAGNOSIS")&&!SymptomsButton.getText().toString().equals("ΠΡΟΣΘΗΚΗ ΣΥΜΠΤΩΜΑΤΩΝ ΚΑΙ ΔΙΑΓΝΩΣΗ")) && ECGExists) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setView(LayoutInflater.from(view.getContext()).inflate(R.layout.requiresymptomsalert, null));
                        AlertDialog dialog = builder.create();
                        dialog.show();

                        TextView symptomsA = (TextView) view.findViewById(R.id.tv_ecg_heading);
                        CheckBox pain = (CheckBox) dialog.findViewById(R.id.checkBox3);
                        CheckBox heartrateAn = (CheckBox) dialog.findViewById(R.id.checkBox4);
                        CheckBox feeling = (CheckBox) dialog.findViewById(R.id.checkBox5);
                        CheckBox efidrosi = (CheckBox) dialog.findViewById(R.id.checkBox6);
                        CheckBox lefthand = (CheckBox) dialog.findViewById(R.id.checkBox7);
                        TextView okSend = (TextView) dialog.findViewById(R.id.buttonOK);

                        if(languageCode==0){
                            symptomsA.setText(getResources().getString(R.string.symptomsAgr));
                            pain.setText(getResources().getString(R.string.paingr));
                            heartrateAn.setText(getResources().getString(R.string.heartgr));
                            feeling.setText(getResources().getString(R.string.lipothimiagr));
                            efidrosi.setText(getResources().getString(R.string.efidrosigr));
                            lefthand.setText(getResources().getString(R.string.moudiasmagr));
                        }else{
                            symptomsA.setText(getResources().getString(R.string.symptomsA));
                            pain.setText(getResources().getString(R.string.pain));
                            heartrateAn.setText(getResources().getString(R.string.heart));
                            feeling.setText(getResources().getString(R.string.lipothimia));
                            efidrosi.setText(getResources().getString(R.string.efidrosi));
                            lefthand.setText(getResources().getString(R.string.moudiasma));
                        }

                        okSend.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (EmailSymptoms == "") {
                                    if (pain.isChecked()) {
                                        EmailSymptoms = EmailSymptoms + pain.getText().toString() + ",";
                                    }
                                    if (heartrateAn.isChecked()) {
                                        EmailSymptoms += heartrateAn.getText().toString() + ",";
                                    }
                                    if (feeling.isChecked()) {
                                        EmailSymptoms += feeling.getText().toString() + ",";
                                    }
                                    if (efidrosi.isChecked()) {
                                        EmailSymptoms += efidrosi.getText().toString() + ",";
                                    }
                                    if (lefthand.isChecked()) {
                                        EmailSymptoms += lefthand.getText().toString();
                                    }
                                }
                                try {
                                    if (!EmailVital.equals("")) {
                                        if(languageCode==0) {
                                            EmailVital = "Μετρήσεις υγείας:" + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailVital;
                                        }else{
                                            EmailVital = "Vital Signs:" + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailVital;
                                        }
                                    }
                                    if (!EmailSymptoms.equals("")) {
                                        if(languageCode==0) {
                                            EmailSymptoms = "Συμπτώματα:" + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailSymptoms;
                                        }else{
                                            EmailSymptoms = "Symptoms:" + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailSymptoms;
                                        }
                                    }
                                    if (!EmailDiagnosis.equals("")) {
                                        if(languageCode==0) {
                                            EmailDiagnosis = "Διάγνωση:" + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailDiagnosis;
                                        }else{
                                            EmailDiagnosis = "Diagnosis:" + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailDiagnosis;
                                        }
                                    }

                                    if (!commentsText.getText().equals("")) {
                                        if(languageCode==0) {
                                            ECGcomments = "Σχόλια:" + System.getProperty("line.separator") + System.getProperty("line.separator") + commentsText.getText().toString();
                                        }else{
                                            ECGcomments = "Comments:" + System.getProperty("line.separator") + System.getProperty("line.separator") + commentsText.getText().toString();
                                        }
                                    }
                                    Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                                    intent.setType("text/plain");
                                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{doctorEmail});
                                    if(languageCode==0) {
                                        intent.putExtra(Intent.EXTRA_SUBJECT, "Δεδομένα υγείας για ασθενή " + patientName);
                                    }else{
                                        intent.putExtra(Intent.EXTRA_SUBJECT, "Health data for Patient " + patientName);
                                    }
                                    intent.putExtra(Intent.EXTRA_TEXT, locationSend+ECGcomments + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailVital + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailSymptoms + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailDiagnosis);

                                    uris = new ArrayList<Uri>();

                                    if (ECGAnalysis) {
                                        Uri uri = Uri.fromFile(new File(getActivity().getExternalFilesDir(null).getAbsolutePath() + "/ECGAnalysis.pdf"));
                                        uris.add(uri);
                                    } else if (ECGExists) {
                                        Uri uri = Uri.fromFile(new File(getActivity().getExternalFilesDir(null).getAbsolutePath() + "/ecg.pdf"));
                                        uris.add(uri);
                                    }
                                    if (selectedImage != "") {
                                        Uri uri = Uri.fromFile(new File(selectedImage));
                                        uris.add(uri);
                                    }

                                    if(uris.size()>0) {
                                        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
                                    }

                                    getActivity().setResult(RESULT_OK, intent);
                                    if(languageCode==0) {
                                        startActivity(Intent.createChooser(intent, "Μοίρασε τα δεδομένα σου"));
                                    }else{
                                        startActivity(Intent.createChooser(intent, "Share your data"));
                                    }
                                    commentsText.setText("");
                                    SendEmail = false;
                                    ECGExists = false;
                                    selectedImage = "";
                                    EmailSymptoms = "";

                                    if(languageCode==0) {
                                        ECGButton.setText(getResources().getString(R.string.tv_start_ecggr));
                                        VitalsButton.setText(getResources().getString(R.string.tv_start_symptomsgr));
                                        SymptomsButton.setText(getResources().getString(R.string.tv_start_vital_notesgr));
                                    }else{
                                        ECGButton.setText(getResources().getString(R.string.tv_start_ecg));
                                        VitalsButton.setText(getResources().getString(R.string.tv_start_symptoms));
                                        SymptomsButton.setText(getResources().getString(R.string.tv_start_vital_notes));
                                    }

                                    ECGImage.setImageResource(R.drawable.ic_heart);
                                    PhotoImage.setImageResource(R.drawable.ic_notes2);
                                    VitalsImage.setImageResource(R.drawable.ic_pulse);
                                    SymptomsImage.setImageResource(R.drawable.ic_symptom);
                                    ECGAnalysis = false;
                                    EmailVital = "";
                                    EmailDiagnosis = "";
                                    locationSend="";
                                    bloodPressure1 = "";
                                    bloodPressure2 = "";
                                    oxygen = "";
                                    weight = "";
                                    heartrate = "";
                                    temp = "";
                                    gluco = "";
                                    //  showToast(getContext(), "Email Send");
                                } catch (Exception e) {

                                }
                                dialog.dismiss();
                            }
                        });
                    } else {
                        try {
                            if (!EmailVital.equals("")) {
                                if(languageCode==0) {
                                    EmailVital = "Μετρήσεις υγείας:" + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailVital;
                                }else{
                                    EmailVital = "Vital Signs:" + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailVital;
                                }
                            }
                            if (!EmailSymptoms.equals("")) {
                                if(languageCode==0) {
                                    EmailSymptoms = "Συμπτώματα:" + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailSymptoms;
                                }else{
                                    EmailSymptoms = "Symptoms:" + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailSymptoms;
                                }
                            }
                            if (!EmailDiagnosis.equals("")) {
                                if(languageCode==0) {
                                    EmailDiagnosis = "Διάγνωση:" + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailDiagnosis;
                                }else{
                                    EmailDiagnosis = "Diagnosis:" + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailDiagnosis;
                                }
                            }

                            if (!commentsText.getText().toString().equals("")) {
                                if(languageCode==0) {
                                    ECGcomments = "Σχόλια:" + System.getProperty("line.separator") + System.getProperty("line.separator") + commentsText.getText().toString();
                                }else{
                                    ECGcomments = "Comments:" + System.getProperty("line.separator") + System.getProperty("line.separator") + commentsText.getText().toString();
                                }
                            }

                            Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                            intent.setType("text/plain");

                            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{doctorEmail});

                            if(languageCode==0) {
                                intent.putExtra(Intent.EXTRA_SUBJECT, "Δεδομένα υγείας για ασθενή " + patientName);
                            }else{
                                intent.putExtra(Intent.EXTRA_SUBJECT, "Health data for Patient " + patientName);
                            }

                            intent.putExtra(Intent.EXTRA_TEXT, locationSend+ECGcomments + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailVital + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailSymptoms + System.getProperty("line.separator") + System.getProperty("line.separator") + EmailDiagnosis);
                            uris = new ArrayList<Uri>();

                            if (ECGAnalysis) {
                                Uri uri = Uri.fromFile(new File(getActivity().getExternalFilesDir(null).getAbsolutePath() + "/ECGAnalysis.pdf"));
                                uris.add(uri);
                            } else if (ECGExists) {
                                Uri uri = Uri.fromFile(new File(getActivity().getExternalFilesDir(null).getAbsolutePath() + "/ecg.pdf"));
                                uris.add(uri);
                            }

                            if (!selectedImage.equals("")) {
                                Uri uri = Uri.fromFile(new File(selectedImage));
                                uris.add(uri);
                            }
                            if(uris.size()>0) {
                                intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
                            }

                            getActivity().setResult(RESULT_OK, intent);
                            if(languageCode==0) {
                                startActivity(Intent.createChooser(intent, "Μοίρασε τα δεδομένα σου"));
                            }else{
                                startActivity(Intent.createChooser(intent, "Share your data"));
                            }
                            commentsText.setText("");
                            SendEmail = false;
                            ECGExists = false;
                            selectedImage = "";
                            EmailSymptoms = "";
                            if(languageCode==0) {
                                ECGButton.setText(getResources().getString(R.string.tv_start_ecggr));
                                VitalsButton.setText(getResources().getString(R.string.tv_start_vital_notesgr));
                                SymptomsButton.setText(getResources().getString(R.string.tv_start_symptomsgr));
                            }else{
                                ECGButton.setText(getResources().getString(R.string.tv_start_ecg));
                                VitalsButton.setText(getResources().getString(R.string.tv_start_vital_notes));
                                SymptomsButton.setText(getResources().getString(R.string.tv_start_symptoms));
                            }
                            ECGImage.setImageResource(R.drawable.ic_heart);
                            PhotoImage.setImageResource(R.drawable.ic_notes2);
                            VitalsImage.setImageResource(R.drawable.ic_pulse);
                            SymptomsImage.setImageResource(R.drawable.ic_symptom);
                            ECGAnalysis = false;
                            EmailVital = "";
                            locationSend="";
                            EmailDiagnosis = "";
                            bloodPressure1 = "";
                            bloodPressure2 = "";
                            oxygen = "";
                            weight = "";
                            heartrate = "";
                            temp="";
                            gluco = "";
                        } catch (Exception e) {
                        }
                    }
                }
            }
        });

        IschemiaScreenNumber = 0;

        ECGButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ECGButton.getText().toString().equals("START ECG")|| ECGButton.getText().toString().equals("ΞΕΚΙΝΗΣΤΕ")) {

                    File fdelete = new File(getActivity().getExternalFilesDir(null).getAbsolutePath() + "/ECGData.txt");
                    if (fdelete.exists()) {
                        if (fdelete.delete()) {
                        }
                    }

                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setView(LayoutInflater.from(view.getContext()).inflate(R.layout.ecg_category_alert, null));
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    TextView ECGTEX = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
                    TextView Arrythmia = (TextView) dialog.findViewById(R.id.tv_check_arrhythmia);
                    TextView Ischemia = (TextView) dialog.findViewById(R.id.tv_check_ischemia);
                    TextView ANSEvaluation = (TextView) dialog.findViewById(R.id.tv_check_arrhythmia2);

                    if(languageCode==0){
                        ECGTEX.setText(getResources().getString(R.string.tv_ecg_headingagr));
                        Arrythmia.setText(getResources().getString(R.string.tv_check_arrhythmiagr));
                        Ischemia.setText(getResources().getString(R.string.tv_check_ischemiagr));
                        ANSEvaluation.setText(getResources().getString(R.string.tv_check_arrhythmia2gr));
                    }else{
                        ECGTEX.setText(getResources().getString(R.string.tv_ecg_headinga));
                        Arrythmia.setText(getResources().getString(R.string.tv_check_arrhythmia));
                        Ischemia.setText(getResources().getString(R.string.tv_check_ischemia));
                        ANSEvaluation.setText(getResources().getString(R.string.tv_check_arrhythmia2));
                    }

                    ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);

                    CancelAlertBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    Arrythmia.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ECGType = 1;
                            ECGTime = 30;
                            IschemiaScreenNumber = 0;
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    });
                    Ischemia.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ECGType = 3;
                            ECGTime = 15;
                            IschemiaScreenNumber = 0;
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    });
                    ANSEvaluation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ECGType = 5;
                            ECGTime = 120;
                            IschemiaScreenNumber = 0;
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    });
                }

                if (ECGButton.getText().toString().equals("GET ECG ANALYSIS")||ECGButton.getText().toString().equals("ΑΝΑΛΥΣΗ ΗΛΚ")) {
                    if (isInternetAvailable()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setView(LayoutInflater.from(view.getContext()).inflate(R.layout.check_ecg_alert, null));
                        AlertDialog dialog = builder.create();
                        dialog.show();

                        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
                        TextView CheckECG = (TextView) dialog.findViewById(R.id.tv_check_arrhythmia);
                        TextView CheckAnalysis = (TextView) dialog.findViewById(R.id.tv_check_ischemia);
                        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);

                        if(languageCode==0){
                            options.setText(getResources().getString(R.string.optionsgr));
                            CheckECG.setText(getResources().getString(R.string.viewgr));
                            CheckAnalysis.setText(getResources().getString(R.string.analysisgr));
                        }else{
                            options.setText(getResources().getString(R.string.options));
                            CheckECG.setText(getResources().getString(R.string.view));
                            CheckAnalysis.setText(getResources().getString(R.string.analysis));
                        }


                        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        CheckECG.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                File file = new File(getActivity().getExternalFilesDir(null).getAbsolutePath() + "/ecg.pdf");
                                Intent target = new Intent(Intent.ACTION_VIEW);
                                target.setDataAndType(Uri.fromFile(file), "application/pdf");
                                target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                Intent intent = Intent.createChooser(target, "Open File");
                                try {
                                    startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                    Toast.makeText(getActivity(), "No PDF Viewer Installed", Toast.LENGTH_LONG).show();
                                }
                                dialog.dismiss();
                            }
                        });

                        CheckAnalysis.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                BorsamHttp.init("189672", "http://cnapp.wecardio.com:28090/", "xlecg896");
                                getConfig();
                                dialog.dismiss();
                            }
                        });
                    } else {
                        if(languageCode==0) {
                            showToast(view.getContext(), "Παρακαλώ συνδεθείτε στο Ιντερνετ");
                        }else{
                            showToast(view.getContext(), "Please connect to the internet");
                        }
                    }
                }

                if (ECGButton.getText().toString().equals("VIEW ECG ANALYSIS")||ECGButton.getText().toString().equals("ΔΕΙΤΕ ΑΝΑΛΥΣΗ ΗΛΚ")) {
                    File file = new File(getActivity().getExternalFilesDir(null).getAbsolutePath() + "/ECGAnalysis.pdf");
                    Intent target = new Intent(Intent.ACTION_VIEW);
                    target.setDataAndType(Uri.fromFile(file), "application/pdf");
                    try {
                        startActivity(target);
                    } catch (ActivityNotFoundException e) {
                    }
                }
            }
        });

        VitalsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreateMeasurement.class);
                startActivity(intent);
            }
        });

        PhotoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setView(LayoutInflater.from(view.getContext()).inflate(R.layout.photoalert, null));
                AlertDialog dialog = builder.create();
                dialog.show();
                TextView cate = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
                TextView selectImage = (TextView) dialog.findViewById(R.id.selectImage);
            //    TextView analyseImage = (TextView) dialog.findViewById(R.id.analyseImage);
           //     TextView captureImage = (TextView) dialog.findViewById(R.id.captureImage);

                if(languageCode==0){
                    cate.setText(getResources().getString(R.string.photogr));
                    selectImage.setText(getResources().getString(R.string.selectImagegr));
              //      analyseImage.setText(getResources().getString(R.string.analyseImagegr));
              //      captureImage.setText(getResources().getString(R.string.captureImagegr));
                }else{
                    cate.setText(getResources().getString(R.string.photo));
                    selectImage.setText(getResources().getString(R.string.selectImage));
              //      analyseImage.setText(getResources().getString(R.string.analyseImage));
              //      captureImage.setText(getResources().getString(R.string.captureImage));
                }

                ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);

                CancelAlertBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                selectImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, RESULT_LOAD_IMAGE);
                        dialog.dismiss();
                    }
                });
/*
                analyseImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                captureImage.setOnClickListener(new View.OnClickListener() {
                    @Override

                    public void onClick(View v) {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                        dialog.dismiss();
                    }
                });
                */
            }
        });

        SymptomsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SymptomsButton.getText().toString().equals("ADD SYMPTOMS OR GET DIAGNOSIS")&&!SymptomsButton.getText().toString().equals("ΠΡΟΣΘΗΚΗ ΣΥΜΠΤΩΜΑΤΩΝ ΚΑΙ ΔΙΑΓΝΩΣΗ")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setView(LayoutInflater.from(view.getContext()).inflate(R.layout.symptoms_category_alert, null));
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
                    TextView sym = (TextView) dialog.findViewById(R.id.tv_check_arrhythmia);
                  //  TextView covid = (TextView) dialog.findViewById(R.id.tv_check_ischemia);
                    ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
                    CancelAlertBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    if(languageCode==0){
                        options.setText(getResources().getString(R.string.symptomsoptionsgr));
                        sym.setText(getResources().getString(R.string.addsymptomsgr));
                      //  covid.setText(getResources().getString(R.string.covidgr));
                    }else{
                        options.setText(getResources().getString(R.string.symptomsoptions));
                        sym.setText(getResources().getString(R.string.addsymptoms));
                    //    covid.setText(getResources().getString(R.string.covid));
                    }

                    sym.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), symptomsActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    });
/*
                    covid.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            covid0(v);
                            dialog.dismiss();
                        }
                    });
                    */
                } else {

                    if(!EmailDiagnosis.equals("")){
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(view.getContext());
                        builder.setMessage(EmailDiagnosis)
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                        android.app.AlertDialog alert = builder.create();
                        alert.show();
                        return;
                    }
                    Intent intent = new Intent(getActivity(), Wallet.class);
                    startActivity(intent);

                    /*
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setView(LayoutInflater.from(view.getContext()).inflate(R.layout.symptoms_alert_design, null));
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    TextView sympalertrecyclerview = (TextView) dialog.findViewById(R.id.symp_alert_recycler_view);
                    sympalertrecyclerview.setMovementMethod(new ScrollingMovementMethod());

                    TextView sympalertproceedtodiagnosis = (TextView) dialog.findViewById(R.id.symp_alert_proceed_to_diagnosis);
                    ImageView sympalertcrossimage = (ImageView) dialog.findViewById(R.id.symp_alert_cross_image);
                    sympalertrecyclerview.setText(Home.EmailSymptoms.replace(",", "\n"));
                    TextView symp_alert_heading_symptoms = (TextView) dialog.findViewById(R.id.symp_alert_heading_symptoms);

                    if(languageCode==0){
                        sympalertrecyclerview.setText(getResources().getString(R.string.in_order_to_get_a_diagnosis_please_proceed_to_the_paymentgr));
                        symp_alert_heading_symptoms.setText(getResources().getString(R.string.symptomsgr));
                    }else{
                        sympalertrecyclerview.setText(getResources().getString(R.string.in_order_to_get_a_diagnosis_please_proceed_to_the_payment));
                        symp_alert_heading_symptoms.setText(getResources().getString(R.string.symptoms));
                    }

                     sympalertproceedtodiagnosis.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), Wallet.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    });
                    sympalertcrossimage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    */
                }
            }
        });

        ECGImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ECGButton.getText().toString().equals("START ECG")||ECGButton.getText().toString().equals("ΞΕΚΙΝΗΣΤΕ")) {
                    File fdelete = new File(getActivity().getExternalFilesDir(null).getAbsolutePath() + "/ECGData.txt");
                    if (fdelete.exists()) {
                        if (fdelete.delete()) {
                            System.out.println("file Deleted :");
                        }
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setView(LayoutInflater.from(view.getContext()).inflate(R.layout.ecg_category_alert, null));
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    TextView Arrythmia = (TextView) dialog.findViewById(R.id.tv_check_arrhythmia);
                    TextView Ischemia = (TextView) dialog.findViewById(R.id.tv_check_ischemia);
                    TextView ANSEvaluation = (TextView) dialog.findViewById(R.id.tv_check_arrhythmia2);
                    ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
                    TextView ECGTEX = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
                    if(languageCode==0){
                        ECGTEX.setText(getResources().getString(R.string.tv_ecg_headingagr));
                        Arrythmia.setText(getResources().getString(R.string.tv_check_arrhythmiagr));
                        Ischemia.setText(getResources().getString(R.string.tv_check_ischemiagr));
                        ANSEvaluation.setText(getResources().getString(R.string.tv_check_arrhythmia2gr));
                    }else{
                        ECGTEX.setText(getResources().getString(R.string.tv_ecg_headinga));
                        Arrythmia.setText(getResources().getString(R.string.tv_check_arrhythmiagr));
                        Ischemia.setText(getResources().getString(R.string.tv_check_ischemiagr));
                        ANSEvaluation.setText(getResources().getString(R.string.tv_check_arrhythmia2gr));
                    }
                    CancelAlertBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    Arrythmia.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ECGType = 1;
                            ECGTime = 30;
                            IschemiaScreenNumber = 0;
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    });
                    Ischemia.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ECGType = 3;
                            ECGTime = 15;
                            IschemiaScreenNumber = 0;
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    });
                    ANSEvaluation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ECGType = 5;
                            ECGTime = 120;
                            IschemiaScreenNumber = 0;
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    });
                }

                if (ECGButton.getText().toString().equals("GET ECG ANALYSIS")||ECGButton.getText().toString().equals("ΑΝΑΛΗΣΗ ΗΛΚ")) {
                    if (isInternetAvailable()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setView(LayoutInflater.from(view.getContext()).inflate(R.layout.check_ecg_alert, null));
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        TextView CheckECG = (TextView) dialog.findViewById(R.id.tv_check_arrhythmia);
                        TextView CheckAnalysis = (TextView) dialog.findViewById(R.id.tv_check_ischemia);
                        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);

                        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        CheckECG.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                File file = new File(getActivity().getExternalFilesDir(null).getAbsolutePath() + "/ecg.pdf");
                                Intent target = new Intent(Intent.ACTION_VIEW);
                                target.setDataAndType(Uri.fromFile(file), "application/pdf");
                                target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                Intent intent = Intent.createChooser(target, "Open File");
                                try {
                                    startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                }
                                dialog.dismiss();
                            }
                        });

                        CheckAnalysis.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                BorsamHttp.init("189672", "http://cnapp.wecardio.com:28090/", "xlecg896");
                                getConfig();
                                dialog.dismiss();
                            }
                        });
                    } else {
                        if(languageCode==0) {
                            showToast(view.getContext(), "Παρακαλώ συνδεθείτε στο Ιντερνετ");
                        }else{
                            showToast(view.getContext(), "Please connect to the internet");
                        }
                    }
                }

                if (ECGButton.getText().toString().equals("VIEW ECG ANALYSIS")||ECGButton.getText().toString().equals("ΔΕΙΤΕ ΑΝΑΛΥΣΗ ΗΛΚ")) {
                    File file = new File(getActivity().getExternalFilesDir(null).getAbsolutePath() + "/ECGAnalysis.pdf");
                    Intent target = new Intent(Intent.ACTION_VIEW);
                    target.setDataAndType(Uri.fromFile(file), "application/pdf");
                    target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    Intent intent = Intent.createChooser(target, "Open File");
                    try {
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                    }
                }

            }
        });

        VitalsImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreateMeasurement.class);
                startActivity(intent);
            }
        });

        SymptomsImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SymptomsButton.getText().toString().equals("ADD SYMPTOMS OR GET DIAGNOSIS")&&!SymptomsButton.getText().toString().equals("ΠΡΟΣΘΗΚΗ ΣΥΜΠΤΩΜΑΤΩΝ ΚΑΙ ΔΙΑΓΝΩΣΗ")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setView(LayoutInflater.from(view.getContext()).inflate(R.layout.symptoms_category_alert, null));
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
                    TextView sym = (TextView) dialog.findViewById(R.id.tv_check_arrhythmia);
                  //  TextView covid = (TextView) dialog.findViewById(R.id.tv_check_ischemia);
                    ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
                    CancelAlertBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    if(languageCode==0){
                        options.setText(getResources().getString(R.string.symptomsoptionsgr));
                        sym.setText(getResources().getString(R.string.addsymptomsgr));
                  //      covid.setText(getResources().getString(R.string.covidgr));
                    }else{
                        options.setText(getResources().getString(R.string.symptomsoptions));
                        sym.setText(getResources().getString(R.string.addsymptoms));
                  //      covid.setText(getResources().getString(R.string.covid));
                    }

                    sym.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), symptomsActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    });
/*
                    covid.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            covid0(v);
                            dialog.dismiss();
                        }
                    });
*/
                } else {
                    Intent intent = new Intent(getActivity(), Wallet.class);
                    startActivity(intent);
                    /*
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setView(LayoutInflater.from(view.getContext()).inflate(R.layout.symptoms_alert_design, null));
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    TextView sympalertrecyclerview = (TextView) dialog.findViewById(R.id.symp_alert_recycler_view);
                    sympalertrecyclerview.setMovementMethod(new ScrollingMovementMethod());
                    TextView sympalertproceedtodiagnosis = (TextView) dialog.findViewById(R.id.symp_alert_proceed_to_diagnosis);
                    ImageView sympalertcrossimage = (ImageView) dialog.findViewById(R.id.symp_alert_cross_image);
                    sympalertrecyclerview.setText(Home.EmailSymptoms.replace(",", "\n"));
                    sympalertproceedtodiagnosis.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), Wallet.class);
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    });
                    sympalertcrossimage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    */
                }
            }
        });

        return view;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getActivity().getContentResolver() != null) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(onDownloadComplete);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 120: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if (result.get(0) != null || result.get(0) != "") {
                        commentsText.setText(result.get(0));
                    }
                }
                break;
            }

            case 0001:{
                if (resultCode == RESULT_OK && null != data) {
                    fusedLocationClient =  LocationServices.getFusedLocationProviderClient(getActivity());
                    getLocation();
                }
                break;
            }
            case 11234:{
                if (resultCode == RESULT_OK && null != data) {
                    selectedImage = getPathFromURI(data.getData());
                    PhotoImage.setImageResource(R.drawable.ic_notes);
                    SendEmail = true;
                }
                break;
            }
            case 132: {
                if (resultCode == RESULT_OK && null != data) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    Uri tempUri = getImageUri(getContext(), photo);
                    selectedImage = getRealPathFromURI(tempUri);
                    PhotoImage.setImageResource(R.drawable.ic_notes);
                    SendEmail = true;
                }
                break;
            }
        }
    }

    private void getLocation(){
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            try{
                                Geocoder geo = new Geocoder(getContext(), Locale.getDefault());
                                List<Address> addresses = geo.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                                if (!addresses.isEmpty()) {
                                    if (addresses.size() > 0) {
                                        if(languageCode==0){
                                            locationSend="Τοποθεσία:\n"+ addresses.get(0).getAddressLine(0)+"\n\n";
                                        }else{
                                            locationSend="Location:\n"+ addresses.get(0).getAddressLine(0)+"\n\n";      }
                                    }
                                }
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    private void startVoiceInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        if(languageCode==0) {
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "ΠΕΙΤΕ ΤΑ ΣΧΟΛΙΑ ΣΑΣ");
        }else{
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "COMMENTS");
        }
        try {
            startActivityForResult(intent, 120);
        } catch (ActivityNotFoundException a) {

        }
    }

    private void getConfig() {
        BorsamRequest request = BorsamHttp.getConfig();
        HttpUtil.getApi().getConfig(request.getUrl(), request.getParams())
                .enqueue(new Callback<BorsamResult<Config>>() {
                    @Override
                    public void onResponse(Call<BorsamResult<Config>> call,
                                           Response<BorsamResult<Config>> response) {
                        HttpUtil.config = response.body().getEntity(Config.class);
                        login("fropapag@gmail.com", "0406040");
                    }

                    @Override
                    public void onFailure(Call<BorsamResult<Config>> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private void login(CharSequence account, CharSequence password) {
        if (TextUtils.isEmpty(account) || TextUtils.isEmpty(password)) {
            return;
        }
        if(languageCode==0){
            showToast(getContext(), "Ανέβασμα αρχείου ΗΛΚ. Παρακαλώ περιμένετε");
        }else {
            showToast(getContext(), "Uploading ECG file For Analysis. Please Wait...");
        }
        Map<String, String> params = new HashMap<>();
        params.put("login_key", account.toString());
        params.put("password", MD5Utils.MD5(password.toString()));
        params.put("app_id", "189672");
        BorsamRequest request = BorsamHttp.getBorsamRequest(HttpUtil.config.getPat_login(), params);
        HttpUtil.getApi().login(request.getUrl(), request.getParams())
                .enqueue(new Callback<BorsamResult<LoginResult>>() {
                    @Override
                    public void onResponse(Call<BorsamResult<LoginResult>> call,
                                           Response<BorsamResult<LoginResult>> response) {
                        if (response.body().isSuccessful()) {
                            LoginResult loginResult = response.body().getEntity(LoginResult.class);
                            BorsamHttp.loginSuccess(loginResult.getUser().getId(), loginResult.getToken());
                            //  editAccount();
                            uploadFile();
                        }
                    }

                    @Override
                    public void onFailure(Call<BorsamResult<LoginResult>> call, Throwable t) {
                        t.printStackTrace();
                        if(languageCode==0){
                            showToast(getContext(), "Πρόβλημα στην υπηρεσία την ανάλυσης");
                        }else {
                            showToast(getContext(), "Analysis Webservice Error");
                        }
                    }
                });
    }

    private void editAccount() {
        Map<String, String> params = new HashMap<>();
        params.put("sex", "2");
        BorsamRequest borsamRequest = BorsamHttp
                .getSignedBorsamRequest(HttpUtil.config.getPat_modify(), params);
        HttpUtil.getApi().post(borsamRequest.getUrl(), borsamRequest.getParams())
                .enqueue(new Callback<BorsamResult>() {
                    @Override
                    public void onResponse(Call<BorsamResult> call, Response<BorsamResult> response) {
                        String resp = response.body().toString();
                        Log.d("ANTONIS SERVICE", resp);
                    }

                    @Override
                    public void onFailure(Call<BorsamResult> call, Throwable t) {
                        t.printStackTrace();
                        Log.d("ANTONIS SERVICE", "Error");
                        showToast(getContext(), "Analysis Webservice Error");
                    }
                });
    }

    private void uploadFile() {
        try {
            File in = new File(getActivity().getExternalFilesDir(null).getAbsolutePath() + "/ECGData.txt");
            InputStream file = new FileInputStream(in);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = file.read(buffer)) != -1) {
                baos.write(buffer, 0, len);
            }
            byte[] b = baos.toByteArray();
            file.close();
            baos.close();
            RequestBody body = RequestBody.create(MediaType.parse("multipart/form-data"), b);
            uploadFile(body);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void isFileExists(){
        File folder1 = new File(getActivity().getExternalFilesDir(null).getAbsolutePath() + "/ECGAnalysis.pdf");
        if(folder1.exists()){
            folder1.delete();
        }
    }

    private void addRecord(String fileNo) {
        if (!TextUtils.isEmpty(fileNo)) {
            if(languageCode==0){
                showToast(getContext(), "Ανέβηκε το αρχείο ΗΛΚ. Παρακαλώ περιμένετε για την ανάλυση");
            }else {
                showToast(getContext(), "ECG File Uploaded. Please Wait For Analysis Result...");
            }
        }
        Map<String, String> params = new HashMap<>();
        params.put("file_no", fileNo);
        params.put("test_time", String.valueOf(System.currentTimeMillis() / 1000L));
        params.put("type", String.valueOf(ECGType));
        params.put("condition", "Good Condition");
        BorsamRequest borsamRequest = BorsamHttp
                .getSignedBorsamRequest(HttpUtil.config.getAdd_record(), params);
        HttpUtil.getApi().post(borsamRequest.getUrl(), borsamRequest.getParams())
                .enqueue(new Callback<BorsamResult>() {
                    @Override
                    public void onResponse(Call<BorsamResult> call, Response<BorsamResult> response) {
                        String resp = response.body().toString();
                        resp = resp.substring(resp.indexOf("file_report"));
                        resp = resp.substring(14, resp.indexOf("pdf") + 3).replace("\\", "");
                        isFileExists();
                        downloadFile(resp, new File(getActivity().getExternalFilesDir(null).getAbsolutePath() + "/ECGAnalysis.pdf"));
                    }

                    @Override
                    public void onFailure(Call<BorsamResult> call, Throwable t) {
                        t.printStackTrace();
                        if(languageCode==0){
                            showToast(getContext(), "Πρόβλημα στην υπηρεσία την ανάλυσης");
                        }else {
                            showToast(getContext(), "Analysis Webservice Error");
                        }
                    }
                });
    }

    private void downloadFile(String url, File outputFile) {
        DownloadManager.Request request=new DownloadManager.Request(Uri.parse(url))
                .setTitle("ECG Analysis")// Title of the Download Notification
                .setDescription("Downloading")// Description of the Download Notification
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)// Visibility of the download Notification
                .setDestinationUri(Uri.fromFile(outputFile))// Uri of the destination file
                .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                .setAllowedOverRoaming(true);// Set if download is allowed on roaming network
        DownloadManager downloadManager= (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
        downloadID = downloadManager.enqueue(request);// en
       /*
        try {
            URL u = new URL(url);
            URLConnection conn = u.openConnection();
            int contentLength = conn.getContentLength();
            DataInputStream stream = new DataInputStream(u.openStream());
            byte[] buffer = new byte[contentLength];
            stream.readFully(buffer);
            stream.close();

            DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
            fos.write(buffer);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
    }

    static String analysisURL;
    static File outputFile;

    private static class HTTPReqTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                URL u = new URL(analysisURL);
                URLConnection conn = u.openConnection();
                int contentLength = conn.getContentLength();
                DataInputStream stream = new DataInputStream(u.openStream());
                byte[] buffer = new byte[contentLength];
                stream.readFully(buffer);
                stream.close();

                DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
                fos.write(buffer);
                fos.flush();
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    private void uploadFile(@NonNull RequestBody body) {
        BorsamRequest borsamRequest = BorsamHttp
                .getSignedBorsamRequest(HttpUtil.config.getUpload_file());
        HttpUtil.getApi().uploadFile(borsamRequest.getUrl(), body)
                .enqueue(new Callback<BorsamResult<UploadResult>>() {
                    @Override
                    public void onResponse(@NonNull Call<BorsamResult<UploadResult>> call,
                                           @NonNull Response<BorsamResult<UploadResult>> response) {
                        if (response.body().isSuccessful()) {
                            UploadResult uploadResult = response.body().getEntity(UploadResult.class);
                            addRecord(uploadResult.getFile_no());
                        }
                    }

                    @Override
                    public void onFailure(Call<BorsamResult<UploadResult>> call, Throwable t) {
                        t.printStackTrace();
                        if(languageCode==0){
                            showToast(getContext(), "Πρόβλημα στην υπηρεσία την ανάλυσης");
                        }else {
                            showToast(getContext(), "Analysis Webservice Error");
                        }
                    }
                });
    }

    void covid0(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.userinfo, null));
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
        RadioGroup sex = (RadioGroup) dialog.findViewById(R.id.radioSex);
        RadioButton male = (RadioButton) dialog.findViewById(R.id.selectImage);
        RadioButton female = (RadioButton) dialog.findViewById(R.id.analyseImage);
        EditText age = (EditText) dialog.findViewById(R.id.captureImage);
        TextView done = (TextView) dialog.findViewById(R.id.captureImage2);
        if(languageCode==0){
            options.setText(getResources().getString(R.string.infogr));
            male.setText(getResources().getString(R.string.malegr));
            female.setText(getResources().getString(R.string.femalegr));
            age.setHint(getResources().getString(R.string.agegr));
        }else{
            options.setText(getResources().getString(R.string.info));
            male.setText(getResources().getString(R.string.male));
            female.setText(getResources().getString(R.string.female));
            age.setHint(getResources().getString(R.string.age));
        }

        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(male.isChecked())
                {
                    sexo="male";
                }
                else
                {
                    sexo="female";
                }

                ageo= age.getText().toString();

                if(ageo.isEmpty()){
                    if(languageCode==0){
                        Toast.makeText(getContext(), "Συμπληρώστε την ηλικία σας", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(getContext(), "Enter your age", Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                dialog.dismiss();
                data="";
                covid1(v);
            }
        });
    }

    void covid1(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        if(languageCode==0){
            builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid1gr, null));
        }else {
            builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid1, null));
        }
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
        TextView done = (TextView) dialog.findViewById(R.id.buttonOK);
        CheckBox check1 = (CheckBox) dialog.findViewById(R.id.checkBox3);
        CheckBox check2 = (CheckBox) dialog.findViewById(R.id.checkBox4);
        CheckBox check3 = (CheckBox) dialog.findViewById(R.id.checkBox5);
        CheckBox check4 = (CheckBox) dialog.findViewById(R.id.checkBox6);
        CheckBox check5 = (CheckBox) dialog.findViewById(R.id.checkBox7);
        CheckBox check6 = (CheckBox) dialog.findViewById(R.id.checkBox8);
        CheckBox check7 = (CheckBox) dialog.findViewById(R.id.checkBox9);
        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check1.isChecked()) { data+="[{\"id\": \"p_16\",\"choice_id\": \"present\"},"; EmailSymptoms+= check1.getText().toString() + ",";} else { data+="[{\"id\": \"p_16\",\"choice_id\": \"absent\"},"; }
                if(check2.isChecked()) { data+="{\"id\": \"p_17\",\"choice_id\": \"present\"},"; EmailSymptoms+= check2.getText().toString() + ",";} else { data+="{\"id\": \"p_17\",\"choice_id\": \"absent\"},"; }
                if(check3.isChecked()) { data+="{\"id\": \"p_18\",\"choice_id\": \"present\"},"; EmailSymptoms+= check3.getText().toString() + ",";} else { data+="{\"id\": \"p_18\",\"choice_id\": \"absent\"},"; }
                if(check4.isChecked()) { data+="{\"id\": \"p_19\",\"choice_id\": \"present\"},"; EmailSymptoms+= check4.getText().toString() + ",";} else { data+="{\"id\": \"p_19\",\"choice_id\": \"absent\"},"; }
                if(check5.isChecked()) { data+="{\"id\": \"p_20\",\"choice_id\": \"present\"},"; EmailSymptoms+= check5.getText().toString() + ",";} else { data+="{\"id\": \"p_20\",\"choice_id\": \"absent\"},"; }
                if(check6.isChecked()) { data+="{\"id\": \"p_21\",\"choice_id\": \"present\"},"; EmailSymptoms+= check6.getText().toString() + ",";} else { data+="{\"id\": \"p_21\",\"choice_id\": \"absent\"},"; }
                if(check7.isChecked()) { data+="{\"id\": \"p_22\",\"choice_id\": \"present\"},"; EmailSymptoms+= check7.getText().toString() + ",";} else { data+="{\"id\": \"p_22\",\"choice_id\": \"absent\"},"; }
                dialog.dismiss();
                Log.d("covid1",data);
                covid2(v);
            }
        });
    }

    void covid2(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        if(languageCode==0){
            builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid2gr, null));
        }else {
            builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid2, null));
        }
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
        CheckBox check1 = (CheckBox) dialog.findViewById(R.id.checkBox3);
        CheckBox check2 = (CheckBox) dialog.findViewById(R.id.checkBox4);
        CheckBox check3 = (CheckBox) dialog.findViewById(R.id.checkBox5);
        TextView done = (TextView) dialog.findViewById(R.id.buttonOK);
        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check1.isChecked()) { data+="{\"id\": \"s_0\",\"choice_id\": \"present\"},"; EmailSymptoms+= check1.getText().toString() + ",";} else { data+="{\"id\": \"s_0\",\"choice_id\": \"absent\"},"; }
                if(check2.isChecked()) { data+="{\"id\": \"s_1\",\"choice_id\": \"present\"},";EmailSymptoms+= check2.getText().toString() + ","; } else { data+="{\"id\": \"s_1\",\"choice_id\": \"absent\"},"; }
                if(check3.isChecked()) { data+="{\"id\": \"s_2\",\"choice_id\": \"present\"},"; EmailSymptoms+= check3.getText().toString() + ",";} else { data+="{\"id\": \"s_2\",\"choice_id\": \"absent\"},"; }
                Log.d("covid2",data);
                if(check1.isChecked()) {
                    covidfever(v);
                }else{
                    covid3(v);
                }
                dialog.dismiss();
            }
        });
    }

    void covidfever(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        if(languageCode==0){
            builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covidfevergr, null));
        }else {
            builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covidfever, null));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
        RadioButton check1 = (RadioButton) dialog.findViewById(R.id.checkBox3);
        RadioButton check2 = (RadioButton) dialog.findViewById(R.id.checkBox4);
        RadioButton check3 = (RadioButton) dialog.findViewById(R.id.checkBox5);
        TextView done = (TextView) dialog.findViewById(R.id.buttonOK);
        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check1.isChecked()) { data+="{\"id\": \"s_3\",\"choice_id\": \"present\"},"; EmailSymptoms+= check1.getText().toString() + ",";} else { data+="{\"id\": \"s_3\",\"choice_id\": \"absent\"},"; }
                if(check2.isChecked()) { data+="{\"id\": \"s_4\",\"choice_id\": \"present\"},";EmailSymptoms+= check2.getText().toString() + ","; } else { data+="{\"id\": \"s_4\",\"choice_id\": \"absent\"},"; }
                if(check3.isChecked()) { data+="{\"id\": \"s_5\",\"choice_id\": \"present\"},"; EmailSymptoms+= check3.getText().toString() + ",";} else { data+="{\"id\": \"s_5\",\"choice_id\": \"absent\"},"; }
                Log.d("covidfever",data);
                dialog.dismiss();
                covid3(v);
            }
        });
    }

    void covid3(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        if(languageCode==0){
            builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid3gr, null));
        }else {
            builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid3, null));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
        CheckBox check1 = (CheckBox) dialog.findViewById(R.id.checkBox3);
        CheckBox check2 = (CheckBox) dialog.findViewById(R.id.checkBox4);
        CheckBox check3 = (CheckBox) dialog.findViewById(R.id.checkBox5);
        TextView done = (TextView) dialog.findViewById(R.id.buttonOK);
        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!check1.isChecked() && !check2.isChecked() && !check3.isChecked()) {
                    if(check1.isChecked()) { data+="{\"id\": \"s_12\",\"choice_id\": \"present\"},"; EmailSymptoms+= check1.getText().toString() + ",";} else { data+="{\"id\": \"s_12\",\"choice_id\": \"absent\"},"; }
                    if(check2.isChecked()) { data+="{\"id\": \"s_13\",\"choice_id\": \"present\"},"; EmailSymptoms+= check2.getText().toString() + ",";} else { data+="{\"id\": \"s_13\",\"choice_id\": \"absent\"},"; }
                    if(check3.isChecked()) { data+="{\"id\": \"s_14\",\"choice_id\": \"present\"},";EmailSymptoms+= check3.getText().toString() + ","; } else { data+="{\"id\": \"s_14\",\"choice_id\": \"absent\"},"; }
                    Log.d("covid3",data);
                    covid4(v);
                }else{
                    if(check1.isChecked()) { data+="{\"id\": \"s_12\",\"choice_id\": \"present\"},";EmailSymptoms+= check1.getText().toString() + ","; } else { data+="{\"id\": \"s_12\",\"choice_id\": \"absent\"},"; }
                    if(check2.isChecked()) { data+="{\"id\": \"s_13\",\"choice_id\": \"present\"},";EmailSymptoms+= check2.getText().toString() + ","; } else { data+="{\"id\": \"s_13\",\"choice_id\": \"absent\"},"; }
                    if(check3.isChecked()) { data+="{\"id\": \"s_14\",\"choice_id\": \"present\"}]";EmailSymptoms+= check3.getText().toString() + ","; } else { data+="{\"id\": \"s_14\",\"choice_id\": \"absent\"}]"; }
                    getDiagnosis(data,sexo,ageo);
                }
                dialog.dismiss();
            }
        });
    }

    void covid4(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        if(languageCode==0){
            builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid4gr, null));
        }else {
            builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid4, null));
        }
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
        CheckBox check1 = (CheckBox) dialog.findViewById(R.id.checkBox3);
        CheckBox check2 = (CheckBox) dialog.findViewById(R.id.checkBox4);
        CheckBox check3 = (CheckBox) dialog.findViewById(R.id.checkBox5);
        CheckBox check4 = (CheckBox) dialog.findViewById(R.id.checkBox6);
        CheckBox check5 = (CheckBox) dialog.findViewById(R.id.checkBox7);
        CheckBox check6 = (CheckBox) dialog.findViewById(R.id.checkBox8);
        CheckBox check7 = (CheckBox) dialog.findViewById(R.id.checkBox9);
        TextView done = (TextView) dialog.findViewById(R.id.buttonOK);
        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check1.isChecked()) { data+="{\"id\": \"s_15\",\"choice_id\": \"present\"},";EmailSymptoms+= check1.getText().toString() + ","; } else { data+="{\"id\": \"s_15\",\"choice_id\": \"absent\"},"; }
                if(check2.isChecked()) { data+="{\"id\": \"s_16\",\"choice_id\": \"present\"},";EmailSymptoms+= check2.getText().toString() + ","; } else { data+="{\"id\": \"s_16\",\"choice_id\": \"absent\"},"; }
                if(check3.isChecked()) { data+="{\"id\": \"s_17\",\"choice_id\": \"present\"},";EmailSymptoms+= check3.getText().toString() + ","; } else { data+="{\"id\": \"s_17\",\"choice_id\": \"absent\"},"; }
                if(check4.isChecked()) { data+="{\"id\": \"s_18\",\"choice_id\": \"present\"},";EmailSymptoms+= check4.getText().toString() + ","; } else { data+="{\"id\": \"s_18\",\"choice_id\": \"absent\"},"; }
                if(check5.isChecked()) { data+="{\"id\": \"s_19\",\"choice_id\": \"present\"},";EmailSymptoms+= check5.getText().toString() + ","; } else { data+="{\"id\": \"s_19\",\"choice_id\": \"absent\"},"; }
                if(check6.isChecked()) { data+="{\"id\": \"s_20\",\"choice_id\": \"present\"},";EmailSymptoms+= check6.getText().toString() + ","; } else { data+="{\"id\": \"s_20\",\"choice_id\": \"absent\"},"; }
                if(check7.isChecked()) { data+="{\"id\": \"s_21\",\"choice_id\": \"present\"},";EmailSymptoms+= check7.getText().toString() + ","; } else { data+="{\"id\": \"s_21\",\"choice_id\": \"absent\"},"; }
                Log.d("covid4",data);
                covid5(v);
                dialog.dismiss();
            }
        });
    }

    void covid5(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        if(languageCode==0){
            builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid5gr, null));
        }else {
            builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.covid5, null));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
        TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
        RadioButton check1 = (RadioButton) dialog.findViewById(R.id.checkBox3);
        RadioButton check2 = (RadioButton) dialog.findViewById(R.id.checkBox4);
        RadioButton check3 = (RadioButton) dialog.findViewById(R.id.checkBox5);
        RadioButton check4 = (RadioButton) dialog.findViewById(R.id.checkBox6);
        RadioButton check5 = (RadioButton) dialog.findViewById(R.id.checkBox7);
        TextView done = (TextView) dialog.findViewById(R.id.buttonOK);
        ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
        CancelAlertBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(check1.isChecked()) { data+="{\"id\": \"p_12\",\"choice_id\": \"present\"},";EmailSymptoms+= check1.getText().toString() + ","; } else { data+="{\"id\": \"p_12\",\"choice_id\": \"absent\"},"; }
                if(check2.isChecked()) { data+="{\"id\": \"p_13\",\"choice_id\": \"present\"},";EmailSymptoms+= check2.getText().toString() + ","; } else { data+="{\"id\": \"p_13\",\"choice_id\": \"absent\"},"; }
                if(check3.isChecked()) { data+="{\"id\": \"p_14\",\"choice_id\": \"present\"},";EmailSymptoms+= check3.getText().toString() + ","; } else { data+="{\"id\": \"p_14\",\"choice_id\": \"absent\"},"; }
                if(check4.isChecked()) { data+="{\"id\": \"p_11\",\"choice_id\": \"present\"},";EmailSymptoms+= check4.getText().toString() + ","; } else { data+="{\"id\": \"p_11\",\"choice_id\": \"absent\"},"; }
                if(check5.isChecked()) { data+="{\"id\": \"p_15\",\"choice_id\": \"present\"}]";EmailSymptoms+= check5.getText().toString() + ","; } else { data+="{\"id\": \"p_15\",\"choice_id\": \"absent\"}]"; }
                Log.d("covid5",data);
                getDiagnosis(data,sexo,ageo);
                dialog.dismiss();
            }
        });
    }

    private void getDiagnosis(String Data,String sex,String age) {
        Log.d("DataToSent",data);
        HttpURLConnection connection = null;
        URL url;
        try {
            url = new URL("https://api.infermedica.com/covid19/triage");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            connection.setRequestProperty("App-Id", "06ecefd2");
            connection.setRequestProperty("App-Key", "7f6f98ab70709a5b87111b5bc791e9ff");
            if(languageCode==0) {
                connection.setRequestProperty("Model", "infermedica-el");
                connection.setRequestProperty("Version", "v3");
            }else{
                connection.setRequestProperty("Model", "infermedica-en");
                connection.setRequestProperty("Version", "v3");
            }
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            String dataToSend = "{\"sex\": \""+sex+"\",\"age\": "+age+",\"evidence\":"+Data+"}";
            Log.d("toSend",dataToSend);

            byte[] bytes = dataToSend.getBytes("UTF-8");
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.write(bytes);
            wr.flush();
            wr.close();
            if (connection.getResponseMessage().equals("OK")) {
                InputStream in = new BufferedInputStream(connection.getInputStream());
                ByteArrayOutputStream buf = new ByteArrayOutputStream();
                int result = in.read();
                while (result != -1) {
                    buf.write((byte) result);
                    result = in.read();
                }
                String response = buf.toString("UTF-8");

                JSONObject reader = new JSONObject(response);
                String diagnosis="";

                if(!reader.getString("description").equals("null")){
                    diagnosis=reader.getString("description");
                }else{
                    if(languageCode==0){
                        diagnosis = "Δεν έχετε μολυνθεί από τον COVID-19";
                    }else {
                        diagnosis = "You have not been infected with COVID-19";
                    }
                }
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getContext());
                builder.setMessage(diagnosis)
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                android.app.AlertDialog alert = builder.create();
                SendEmail=true;
                alert.show();
            } else {
                Toast.makeText(getContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        } catch (UnknownHostException | JSONException e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (ProtocolException e) {
            Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (ConnectException e) {
            Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (MalformedURLException e) {
            Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
