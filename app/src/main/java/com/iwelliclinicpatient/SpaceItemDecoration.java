package com.iwelliclinicpatient;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * @author GreenhairTurtle
 * <br> email:wuyongjie2008@gmail.com
 * @version 1.0
 * @date 2018/4/24  17:00
 * @description RecyclerView的通用间距设置
 **/
public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

  int mLeft, mRight, mTop, mBottom;
  //只有第一项有上间距
  boolean onlyFirstItemHasTopSpace = true;

  @Override
  public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
      RecyclerView.State state) {
    super.getItemOffsets(outRect, view, parent, state);
    outRect.left = mLeft;
    outRect.right = mRight;
    outRect.bottom = mBottom;
    if (onlyFirstItemHasTopSpace && parent.getChildAdapterPosition(view) == 0) {
      outRect.top = mTop;
    } else {
      outRect.top = mTop;
    }

  }

  /**
   * 适用于四周间距一样的构造方法
   * <strong>第一项会有上间距，其它项只有下间距</strong>
   *
   * @param space 间距
   */
  public SpaceItemDecoration(int space) {
    mLeft = mBottom = mTop = mRight = space;
  }

  /**
   * 适用于四周间距一样的构造方法
   * <strong>第一项会有上间距，其它项只有下间距</strong>
   *
   * @param onlyFirstItemHasTopSpace <code>true</code> 只有第一项有上间距 <p><code>false</code> 所有项都有上间距
   * @param space 间距
   */
  public SpaceItemDecoration(int space, boolean onlyFirstItemHasTopSpace) {
    mLeft = mBottom = mTop = mRight = space;
    this.onlyFirstItemHasTopSpace = onlyFirstItemHasTopSpace;
  }

  /**
   * 适用于单个方向有间距且相同，另外一个方向没有间距的情况
   * @param space 如果 <code>param:horizontal</code>为 <code>true</code> 就是横向间距，为 <code>false</code> 就是纵向间距
   * @param onlyFirstItemHasTopSpace <code>true</code> 只有第一项有上间距 <p><code>false</code> 所有项都有上间距
   */
  public SpaceItemDecoration(int space, boolean horizontal, boolean onlyFirstItemHasTopSpace) {
    if (horizontal) {
      mLeft = mRight = space;
    } else {
      mTop = mBottom = space;
    }
    this.onlyFirstItemHasTopSpace = onlyFirstItemHasTopSpace;
  }

  /**
   * 适用于单个方向间距相同，另一方向间距不同的构造方法
   * <strong>第一项会有上间距，其它项只有下间距</strong>
   *
   * @param space 如果<code>param:horizontal</code>为 <code>true</code> 就是横向间距，为 <code>false</code> 就是纵向间距
   * @param topOrLeft 上间距 如果<code>param:horizontal</code>为 <code>true</code> 就是上间距，为 <code>false</code> 就是左间距
   * @param bottomOrRight 下间距 如果<code>param:horizontal</code>为 <code>true</code> 就是下间距，为 <code>false</code> 就是右间距
   */
  public SpaceItemDecoration(int space, int topOrLeft, int bottomOrRight, boolean horizontal) {
    if (horizontal) {
      mLeft = mRight = space;
      mTop = topOrLeft;
      mBottom = bottomOrRight;
    } else {
      mTop = mBottom = space;
      mLeft = topOrLeft;
      mRight = bottomOrRight;
    }
  }

  /**
   * 适用于单个方向间距相同，另一方向间距不同的构造方法
   *
   * @param space 如果<code>param:horizontal</code>为 <code>true</code> 就是横向间距，为 <code>false</code> 就是纵向间距
   * @param topOrLeft 上间距 如果<code>param:horizontal</code>为 <code>true</code> 就是上间距，为 <code>false</code> 就是左间距
   * @param bottomOrRight 下间距 如果<code>param:horizontal</code>为 <code>true</code> 就是下间距，为 <code>false</code> 就是右间距
   * @param onlyFirstItemHasTopSpace <code>true</code> 只有第一项有上间距 <p><code>false</code> 所有项都有上间距
   */
  public SpaceItemDecoration(int space, int topOrLeft, int bottomOrRight, boolean horizontal,
      boolean onlyFirstItemHasTopSpace) {
    if (horizontal) {
      mLeft = mRight = space;
      mTop = topOrLeft;
      mBottom = bottomOrRight;
    } else {
      mTop = mBottom = space;
      mLeft = topOrLeft;
      mRight = bottomOrRight;
    }
    this.onlyFirstItemHasTopSpace = onlyFirstItemHasTopSpace;
  }

  /**
   * 适用于四边间距都不同的构造方法
   * <strong>第一项会有上间距，其它项只有下间距</strong>
   *
   * @param left 左间距
   * @param right 右间距
   * @param top 上间距
   * @param bottom 下间距
   */
  public SpaceItemDecoration(int left, int right, int top, int bottom) {
    mLeft = left;
    mRight = right;
    mTop = top;
    mBottom = bottom;
  }

  /**
   * 适用于四边间距都不同的构造方法
   *
   * @param left 左间距
   * @param right 右间距
   * @param top 上间距
   * @param bottom 下间距
   * @param onlyFirstItemHasTopSpace <code>true</code> 只有第一项有上间距 <p><code>false</code> 所有项都有上间距
   */
  public SpaceItemDecoration(int left, int right, int top, int bottom,
      boolean onlyFirstItemHasTopSpace) {
    mLeft = left;
    mRight = right;
    mTop = top;
    mBottom = bottom;
    this.onlyFirstItemHasTopSpace = onlyFirstItemHasTopSpace;
  }
}