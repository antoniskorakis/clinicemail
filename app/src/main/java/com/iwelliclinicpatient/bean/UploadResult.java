package com.iwelliclinicpatient.bean;

import android.os.Parcel;

/**
 * @author GreenhairTurtle
 * <br> email:wuyongjie2008@gmail.com
 * @version 1.0
 * @date 2018/9/26  15:53
 * @description 上传文件结果
 **/
public class UploadResult implements android.os.Parcelable {

  public static final Creator<UploadResult> CREATOR = new Creator<UploadResult>() {
    @Override
    public UploadResult createFromParcel(Parcel source) {
      return new UploadResult(source);
    }

    @Override
    public UploadResult[] newArray(int size) {
      return new UploadResult[size];
    }
  };
  private String file_no;

  public UploadResult() {
  }

  protected UploadResult(Parcel in) {
    this.file_no = in.readString();
  }

  @Override
  public String toString() {
    return "UploadResult{" +
        "file_no='" + file_no + '\'' +
        '}';
  }

  public String getFile_no() {
    return file_no == null ? "" : file_no;
  }

  public void setFile_no(String file_no) {
    this.file_no = file_no;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.file_no);
  }
}
