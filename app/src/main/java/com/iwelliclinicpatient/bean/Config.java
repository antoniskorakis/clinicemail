package com.iwelliclinicpatient.bean;

/**
 * @author GreenhairTurtle
 * <br> email:wuyongjie2008@gmail.com
 * @version 1.0
 * @date 2019/1/22  17:33
 * @description config 用于demo展示
 **/
public class Config {

  /**
   * pat_register : http://cnapp.wecardio.com:808/company/register
   * pat_login : http://cnapp.wecardio.com:808/company/login
   * pat_face : http://cnapp.wecardio.com:808/company/face
   * pat_modify : http://cnapp.wecardio.com:808/company/modify
   * upload_file : http://cnapp.wecardio.com:808/company/upload
   * add_record : http://cnapp.wecardio.com:808/company/add
   * get_record : http://cnapp.wecardio.com:808/company/list
   * pat_password_reset : http://cnapp.wecardio.com:808/company/password/reset
   * pat_password_set : http://cnapp.wecardio.com:808/company/password/set
   */

  private String pat_register;
  private String pat_login;
  private String pat_face;
  private String pat_modify;
  private String upload_file;
  private String add_record;
  private String get_record;
  private String pat_password_reset;
  private String pat_password_set;

  public String getPat_register() {
    return pat_register == null ? "" : pat_register;
  }

  public void setPat_register(String pat_register) {
    this.pat_register = pat_register;
  }

  public String getPat_login() {
    return pat_login == null ? "" : pat_login;
  }

  public void setPat_login(String pat_login) {
    this.pat_login = pat_login;
  }

  public String getPat_face() {
    return pat_face == null ? "" : pat_face;
  }

  public void setPat_face(String pat_face) {
    this.pat_face = pat_face;
  }

  public String getPat_modify() {
    return pat_modify == null ? "" : pat_modify;
  }

  public void setPat_modify(String pat_modify) {
    this.pat_modify = pat_modify;
  }

  public String getUpload_file() {
    return upload_file == null ? "" : upload_file;
  }

  public void setUpload_file(String upload_file) {
    this.upload_file = upload_file;
  }

  public String getAdd_record() {
    return add_record == null ? "" : add_record;
  }

  public void setAdd_record(String add_record) {
    this.add_record = add_record;
  }

  public String getGet_record() {
    return get_record == null ? "" : get_record;
  }

  public void setGet_record(String get_record) {
    this.get_record = get_record;
  }

  public String getPat_password_reset() {
    return pat_password_reset == null ? "" : pat_password_reset;
  }

  public void setPat_password_reset(String pat_password_reset) {
    this.pat_password_reset = pat_password_reset;
  }

  public String getPat_password_set() {
    return pat_password_set == null ? "" : pat_password_set;
  }

  public void setPat_password_set(String pat_password_set) {
    this.pat_password_set = pat_password_set;
  }

  @Override
  public String toString() {
    return "Config{" +
        "pat_register='" + pat_register + '\'' +
        ", pat_login='" + pat_login + '\'' +
        ", pat_face='" + pat_face + '\'' +
        ", pat_modify='" + pat_modify + '\'' +
        ", upload_file='" + upload_file + '\'' +
        ", add_record='" + add_record + '\'' +
        ", get_record='" + get_record + '\'' +
        ", pat_password_reset='" + pat_password_reset + '\'' +
        ", pat_password_set='" + pat_password_set + '\'' +
        '}';
  }
}
