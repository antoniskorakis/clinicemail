package com.iwelliclinicpatient.bean;

import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.Gson;

/**
 * @author GreenhairTurtle
 * <br> email:wuyongjie2008@gmail.com
 * @version 1.0
 * @date 2019/1/22  17:57
 * @description borsam接口通用结果 demo展示
 **/
public class BorsamResult<T> {

  private int code;
  private String msg;
  private String data;
  private T entity;

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getMsg() {
    return msg == null ? "" : msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public String getData() {
    return data == null ? "" : data;
  }

  public void setData(String data) {
    this.data = data;
  }

  @Nullable
  public T getEntity(Class<T> clazz) {
    if (entity == null && !TextUtils.isEmpty(data)) {
      entity = new Gson().fromJson(data, clazz);
    }
    return entity;
  }

  public void setEntity(T entity) {
    this.entity = entity;
  }

  @Override
  public String toString() {
    return "BorsamResult{" +
        "code=" + code +
        ", msg='" + msg + '\'' +
        ", data='" + data + '\'' +
        ", entity=" + entity +
        '}';
  }

  public boolean isSuccessful() {

    return code == 0;
  }
}
