package com.iwelliclinicpatient.bean;

import android.os.Parcel;

public class Account implements android.os.Parcelable {

    /**
     * id : 21239
     * first_name : 
     * second_name : 
     * birthday : 0
     * mobile : 12345 6
     * email : 
     * sex : 1
     * language_id : 1001
     * language_name : 中文(简体)
     * created : 1527737270
     * oid : 0
     * did : 0
     * bind_type : 2
     * bind_time : 0
     * login_time : 1548208368
     * wallet_state : 1
     * time_zone : 800
     * face_url : 
     * face_thumb : 
     */

    private int id;
    private String first_name;
    private String second_name;
    private int birthday;
    private String mobile;
    private String email;
    private int sex;
    private int language_id;
    private String language_name;
    private int created;
    private int oid;
    private int did;
    private int bind_type;
    private int bind_time;
    private int login_time;
    private int wallet_state;
    private int time_zone;
    private String face_url;
    private String face_thumb;

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }

    public String getFirst_name() {
      return first_name == null ? "" : first_name;
    }

    public void setFirst_name(String first_name) {
      this.first_name = first_name;
    }

    public String getSecond_name() {
      return second_name == null ? "" : second_name;
    }

    public void setSecond_name(String second_name) {
      this.second_name = second_name;
    }

    public int getBirthday() {
      return birthday;
    }

    public void setBirthday(int birthday) {
      this.birthday = birthday;
    }

    public String getMobile() {
      return mobile == null ? "" : mobile;
    }

    public void setMobile(String mobile) {
      this.mobile = mobile;
    }

    public String getEmail() {
      return email == null ? "" : email;
    }

    public void setEmail(String email) {
      this.email = email;
    }

    public int getSex() {
      return sex;
    }

    public void setSex(int sex) {
      this.sex = sex;
    }

    public int getLanguage_id() {
      return language_id;
    }

    public void setLanguage_id(int language_id) {
      this.language_id = language_id;
    }

    public String getLanguage_name() {
      return language_name == null ? "" : language_name;
    }

    public void setLanguage_name(String language_name) {
      this.language_name = language_name;
    }

    public int getCreated() {
      return created;
    }

    public void setCreated(int created) {
      this.created = created;
    }

    public int getOid() {
      return oid;
    }

    public void setOid(int oid) {
      this.oid = oid;
    }

    public int getDid() {
      return did;
    }

    public void setDid(int did) {
      this.did = did;
    }

    public int getBind_type() {
      return bind_type;
    }

    public void setBind_type(int bind_type) {
      this.bind_type = bind_type;
    }

    public int getBind_time() {
      return bind_time;
    }

    public void setBind_time(int bind_time) {
      this.bind_time = bind_time;
    }

    public int getLogin_time() {
      return login_time;
    }

    public void setLogin_time(int login_time) {
      this.login_time = login_time;
    }

    public int getWallet_state() {
      return wallet_state;
    }

    public void setWallet_state(int wallet_state) {
      this.wallet_state = wallet_state;
    }

    public int getTime_zone() {
      return time_zone;
    }

    public void setTime_zone(int time_zone) {
      this.time_zone = time_zone;
    }

    public String getFace_url() {
      return face_url == null ? "" : face_url;
    }

    public void setFace_url(String face_url) {
      this.face_url = face_url;
    }

    public String getFace_thumb() {
      return face_thumb == null ? "" : face_thumb;
    }

    public void setFace_thumb(String face_thumb) {
      this.face_thumb = face_thumb;
    }


  @Override
  public int describeContents() { return 0; }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(this.id);
    dest.writeString(this.first_name);
    dest.writeString(this.second_name);
    dest.writeInt(this.birthday);
    dest.writeString(this.mobile);
    dest.writeString(this.email);
    dest.writeInt(this.sex);
    dest.writeInt(this.language_id);
    dest.writeString(this.language_name);
    dest.writeInt(this.created);
    dest.writeInt(this.oid);
    dest.writeInt(this.did);
    dest.writeInt(this.bind_type);
    dest.writeInt(this.bind_time);
    dest.writeInt(this.login_time);
    dest.writeInt(this.wallet_state);
    dest.writeInt(this.time_zone);
    dest.writeString(this.face_url);
    dest.writeString(this.face_thumb);
  }

  public Account() {}

  protected Account(Parcel in) {
    this.id = in.readInt();
    this.first_name = in.readString();
    this.second_name = in.readString();
    this.birthday = in.readInt();
    this.mobile = in.readString();
    this.email = in.readString();
    this.sex = in.readInt();
    this.language_id = in.readInt();
    this.language_name = in.readString();
    this.created = in.readInt();
    this.oid = in.readInt();
    this.did = in.readInt();
    this.bind_type = in.readInt();
    this.bind_time = in.readInt();
    this.login_time = in.readInt();
    this.wallet_state = in.readInt();
    this.time_zone = in.readInt();
    this.face_url = in.readString();
    this.face_thumb = in.readString();
  }

  public static final Creator<Account> CREATOR = new Creator<Account>() {
    @Override
    public Account createFromParcel(Parcel source) {return new Account(source);}

    @Override
    public Account[] newArray(int size) {return new Account[size];}
  };
}