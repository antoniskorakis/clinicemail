package com.iwelliclinicpatient.bean;

/**
 * @author GreenhairTurtle
 * <br> email:wuyongjie2008@gmail.com
 * @version 1.0
 * @date 2019/1/23  9:54
 * @description 登陆结果 demo展示
 **/
public class LoginResult {

  /**
   * user : {"id":21239,"first_name":"","second_name":"","birthday":0,"mobile":"12345 6","email":"","sex":1,"language_id":1001,"language_name":"中文(简体)","created":1527737270,"oid":0,"did":0,"bind_type":2,"bind_time":0,"login_time":1548208368,"wallet_state":1,"time_zone":800,"f ace_url":"","face_thumb":""}
   * token : 929A54CE029110480508AE20C0476CB1FCC6
   */

  private Account user;
  private String token;

  public Account getUser() {
    return user;
  }

  public void setUser(Account user) {
    this.user = user;
  }

  public String getToken() {
    return token == null ? "" : token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}
