package com.iwelliclinicpatient;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.borsam.ble.BorsamBleManager;
import com.borsam.ble.callback.BorsamBleScanCallback;
import com.borsam.device.BorsamDevice;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST_LOCATION_PERMISSION = 1001;
    private View mActionView;
    private boolean scanning;
    BroadcastReceiver broadcastReceiver;
    int languageCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences sharedPref = getSharedPreferences("USERDATA", MODE_PRIVATE);
        languageCode = sharedPref.getInt("language", 0);

        mActionView = LayoutInflater.from(this).inflate(R.layout.progressbar, null);
        requestPermission();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent intent) {
                String action = intent.getAction();
                if (action.equals("finish_activity")) {
                    unregisterReceiver(this);
                    finish();
                }
            }
        };

       registerReceiver(broadcastReceiver, new IntentFilter("finish_activity"));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.refresh);
        if (menuItem != null) {
            menuItem.setActionView(scanning ? mActionView : null);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                requestPermission();
                break;
            case R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        } else {
            scanDevice();
        }
    }

    private void scanDevice() {
        BorsamBleManager.getInstance().scan(new BorsamBleScanCallback() {
            @Override
            public void onScanStarted(boolean success) {
                if(languageCode==0){
                    Toast.makeText(getApplicationContext(), "Αναζήτηση συσκευής ΗΛΚ", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Searching for ECG Device...", Toast.LENGTH_SHORT).show();
                }
                scanning = success;
                invalidateOptionsMenu();
            }

            @Override
            public void onScanning(BorsamDevice borsamDevice) {
                EcgViewActivity.start(MainActivity.this,borsamDevice);
            }

            @Override
            public void onScanFinished(List<BorsamDevice> list) {
                scanning = false;
                invalidateOptionsMenu();

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISSION && permissions.length >= 1
                && Manifest.permission.ACCESS_FINE_LOCATION.equals(permissions[0]) && grantResults.length >= 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            scanDevice();
        } else {
            if(languageCode==0){
                Toast.makeText(this, "Παρακαλώ ανοίξτε τις υπηρεσίες τοποθεσίας", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this, R.string.please_open_location_permission, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void showToast(Context mContext, String message){
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        unregisterReceiver(broadcastReceiver);
        BorsamBleManager.getInstance().cancelScan();
        BorsamBleManager.getInstance().disconnectAllDevice();
        BorsamBleManager.getInstance().destroy();
        finish();
    }
}
