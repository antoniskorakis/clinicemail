package com.iwelliclinicpatient;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PatientAdapter extends RecyclerView.Adapter<PatientAdapter.ViewHolder> {

    private List<Patient> mPatients;

    // Pass in the contact array into the constructor
    public PatientAdapter(List<Patient> patients) {
        mPatients = patients;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View patientView = inflater.inflate(R.layout.patientrecyclerdesign, parent, false);
        ViewHolder viewHolder = new ViewHolder(patientView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Patient patient = mPatients.get(position);
        TextView textView1 = viewHolder.patient_number;
        TextView textView2 = viewHolder.patient_name;
        textView1.setText(patient.getID());
        textView2.setText(patient.getName());
    }

    @Override
    public int getItemCount() {
        return mPatients.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        public TextView patient_number;
        public TextView patient_name;
        private Context context;

        public ViewHolder(View itemView) {
            super(itemView);
            patient_number = (TextView) itemView.findViewById(R.id.patient_number);
            patient_name = (TextView) itemView.findViewById(R.id.patient_name);
            this.context = context;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition(); // gets item position
            if (position != RecyclerView.NO_POSITION) { // Check if an item was deleted, but the user clicked it before the UI removed it
                Patient patient = mPatients.get(position);
                MeasurementsActivity.selectPatient(patient.getID()+" "+patient.getName());
            }
        }
    }


}
