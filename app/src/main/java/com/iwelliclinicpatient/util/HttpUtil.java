package com.iwelliclinicpatient.util;

import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;
import java.util.concurrent.TimeUnit;

import com.iwelliclinicpatient.BuildConfig;
import com.iwelliclinicpatient.bean.Config;
import com.iwelliclinicpatient.ui.Api;
import okhttp3.OkHttpClient;
import okhttp3.internal.platform.Platform;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author GreenhairTurtle
 * <br> email:wuyongjie2008@gmail.com
 * @version 2.0
 * @date 2018/5/3  15:51
 * @description 网络请求 demo展示
 **/
public class HttpUtil {

  private static OkHttpClient okHttpClient;
  private static Api api;
  public static Config config;

  public static Api getApi() {
    if (api == null) {
      synchronized (HttpUtil.class) {
        if (api == null) {
          setUpOkHttpClient();
          Retrofit retrofit = new Retrofit.Builder()
              .client(okHttpClient)
              .baseUrl("http://cnapp.wecardio.com:28090/")
              .addConverterFactory(GsonConverterFactory.create())
              .build();
          api = retrofit.create(Api.class);
        }
      }
    }
    return api;
  }

  private static void setUpOkHttpClient() {
    if (okHttpClient == null) {
      okHttpClient = new OkHttpClient.Builder()
          .readTimeout(20, TimeUnit.SECONDS)
          .writeTimeout(20, TimeUnit.SECONDS)
          .addInterceptor(new LoggingInterceptor.Builder()
              .loggable(BuildConfig.DEBUG)
              .setLevel(Level.BASIC)
              .log(Platform.INFO)
              .request("Request")
              .response("Response")
              .addHeader("versionName", BuildConfig.VERSION_NAME)
              .addHeader("versionCode", String.valueOf(BuildConfig.VERSION_CODE))
              .addHeader("applicationId", BuildConfig.APPLICATION_ID)
              .build())
          .build();
    }
  }
}
