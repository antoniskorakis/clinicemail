package com.iwelliclinicpatient;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;

public class covidActivity extends AppCompatActivity {

    protected Context mContext;
    int languageCode = 0;
    CardView  general_card_view;
    CardView  general_drop_down_card_view;
    ImageView  general_image_right;
    CardView musculoskeletal_system_card_view;
    CardView  musculoskeletal_system_drop_down_card_view;
    ImageView  musculoskeletal_system_image_right;
    TextView symptom_done_button;

    private void getDiagnosis(String Data,String sex,String age) {
        HttpURLConnection connection = null;
        URL url;
        try {
            url = new URL("https://api.infermedica.com/covid19/triage");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            connection.setRequestProperty("App-Id", "06ecefd2");
            connection.setRequestProperty("App-Key", "7f6f98ab70709a5b87111b5bc791e9ff");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            String dataToSend = "{\"sex\": \""+sex+"\",\"age\": "+age+",\"evidence\":"+Data+"}";
            Log.d("toSend",dataToSend);

            byte[] bytes = dataToSend.getBytes("UTF-8");
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.write(bytes);
            wr.flush();
            wr.close();
            if (connection.getResponseMessage().equals("OK")) {
                InputStream in = new BufferedInputStream(connection.getInputStream());
                ByteArrayOutputStream buf = new ByteArrayOutputStream();
                int result = in.read();
                while (result != -1) {
                    buf.write((byte) result);
                    result = in.read();
                }
                String response = buf.toString("UTF-8");

                JSONObject reader = new JSONObject(response);
                String diagnosis="";

                if(!reader.getString("description").equals("null")){
                    diagnosis=reader.getString("description");
                }else{
                    if(languageCode==0){
                        diagnosis = "Δεν έχετε μολυνθεί από τον COVID-19";
                    }else {
                        diagnosis = "You have not been infected with COVID-19";
                    }
                }
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                    builder.setMessage(diagnosis)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                    android.app.AlertDialog alert = builder.create();
                    alert.show();
                } else {
                Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        } catch (UnknownHostException | JSONException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (ProtocolException e) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (ConnectException e) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (MalformedURLException e) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        SharedPreferences sharedPref = getSharedPreferences("USERDATA", MODE_PRIVATE);
        languageCode = sharedPref.getInt("language", 0);
        if(languageCode==0){
            setContentView(R.layout.covid_drop_downgr);
        }else{
            setContentView(R.layout.covid_drop_down);
        }
        symptom_done_button = (TextView)findViewById(R.id.symptom_done_button);
        general_card_view=findViewById(R.id.general_card_view);
        general_drop_down_card_view=findViewById(R.id.general_drop_down_card_view);
        general_image_right=findViewById(R.id.general_image_right);
        musculoskeletal_system_card_view=findViewById(R.id.musculoskeletal_system_card_view);
        musculoskeletal_system_drop_down_card_view=findViewById(R.id.musculoskeletal_system_drop_down_card_view);
        musculoskeletal_system_image_right=findViewById(R.id.musculoskeletal_system_image_right);
        mContext = this;
        general_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(general_drop_down_card_view.isShown()) {
                    general_drop_down_card_view.setVisibility(View.GONE);
                    general_image_right.animate().rotation(360.00f).start();
                }else {
                    general_drop_down_card_view.setVisibility(View.VISIBLE);
                    general_image_right.animate().rotation(270).start();
                }
            }
        });
        musculoskeletal_system_card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(musculoskeletal_system_drop_down_card_view.isShown()) {
                    musculoskeletal_system_drop_down_card_view.setVisibility(View.GONE);
                    musculoskeletal_system_image_right.animate().rotation(360.00f).start();
                }else {
                    musculoskeletal_system_drop_down_card_view.setVisibility(View.VISIBLE);
                    musculoskeletal_system_image_right.animate().rotation(270).start();
                }
            }
        });

        symptom_done_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setView(LayoutInflater.from(v.getContext()).inflate(R.layout.userinfo, null));
                AlertDialog dialog = builder.create();
                dialog.show();
                TextView options = (TextView) dialog.findViewById(R.id.tv_ecg_heading);
                RadioGroup sex = (RadioGroup) dialog.findViewById(R.id.radioSex);
                RadioButton male = (RadioButton) dialog.findViewById(R.id.selectImage);
                RadioButton female = (RadioButton) dialog.findViewById(R.id.analyseImage);
                EditText age = (EditText) dialog.findViewById(R.id.captureImage);
                TextView done = (TextView) dialog.findViewById(R.id.captureImage2);

                if(languageCode==0){
                    options.setText(getResources().getString(R.string.infogr));
                    male.setText(getResources().getString(R.string.malegr));
                    female.setText(getResources().getString(R.string.femalegr));
                    age.setHint(getResources().getString(R.string.agegr));
                }else{
                    options.setText(getResources().getString(R.string.info));
                    male.setText(getResources().getString(R.string.male));
                    female.setText(getResources().getString(R.string.female));
                    age.setHint(getResources().getString(R.string.age));
                }

                ImageView CancelAlertBox = (ImageView) dialog.findViewById(R.id.iv_cancel_ecg_alert);
                CancelAlertBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String sexo="";
                        String ageo="";

                        if(male.isChecked())
                        {
                            sexo="male";
                        }
                        else
                        {
                            sexo="female";
                        }

                        ageo= age.getText().toString();

                        if(ageo.isEmpty()){
                            if(languageCode==0){
                                Toast.makeText(getApplicationContext(), "Συμπληρώστε την ηλικία σας", Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(getApplicationContext(), "Enter your age", Toast.LENGTH_SHORT).show();
                            }
                            return;
                        }

                        String data = "";

                        if (((CheckBox) findViewById(R.id.general_checkbox_1)).isChecked()) {
                            data += "[{\"id\": \"p_5\",\"choice_id\": \"present\"},";
                        } else {
                            data += "[{\"id\": \"p_5\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.general_checkbox_2)).isChecked()) {
                            data += "{\"id\": \"p_11\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"p_11\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.general_checkbox_3)).isChecked()) {
                            data += "{\"id\": \"p_15\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"p_15\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.general_checkbox_4)).isChecked()) {
                            data += "{\"id\": \"p_12\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"p_12\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.general_checkbox_5)).isChecked()) {
                            data += "{\"id\": \"p_13\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"p_13\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.general_checkbox_6)).isChecked()) {
                            data += "{\"id\": \"p_14\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"p_14\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.general_checkbox_7)).isChecked()) {
                            data += "{\"id\": \"p_16\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"p_16\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.general_checkbox_8)).isChecked()) {
                            data += "{\"id\": \"p_17\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"p_17\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.general_checkbox_9)).isChecked()) {
                            data += "{\"id\": \"p_18\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"p_18\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.general_checkbox_10)).isChecked()) {
                            data += "{\"id\": \"p_19\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"p_19\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.general_checkbox_11)).isChecked()) {
                            data += "{\"id\": \"p_20\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"p_20\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.general_checkbox_12)).isChecked()) {
                            data += "{\"id\": \"p_21\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"p_21\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.general_checkbox_13)).isChecked()) {
                            data += "{\"id\": \"p_22\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"p_22\",\"choice_id\": \"absent\"},";
                        }


                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_1)).isChecked()) {
                            data += "{\"id\": \"s_0\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_0\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_2)).isChecked()) {
                            data += "{\"id\": \"s_1\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_1\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_3)).isChecked()) {
                            data += "{\"id\": \"s_2\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_2\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_4)).isChecked()) {
                            data += "{\"id\": \"s_10\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_10\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_5)).isChecked()) {
                            data += "{\"id\": \"s_11\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_11\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_6)).isChecked()) {
                            data += "{\"id\": \"s_12\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_12\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_7)).isChecked()) {
                            data += "{\"id\": \"s_3\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_3\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_8)).isChecked()) {
                            data += "{\"id\": \"s_4\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_4\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_9)).isChecked()) {
                            data += "{\"id\": \"s_5\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_5\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_10)).isChecked()) {
                            data += "{\"id\": \"s_13\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_13\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_11)).isChecked()) {
                            data += "{\"id\": \"s_14\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_14\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_12)).isChecked()) {
                            data += "{\"id\": \"s_15\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_15\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_13)).isChecked()) {
                            data += "{\"id\": \"s_16\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_16\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_14)).isChecked()) {
                            data += "{\"id\": \"s_17\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_17\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_15)).isChecked()) {
                            data += "{\"id\": \"s_18\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_18\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_16)).isChecked()) {
                            data += "{\"id\": \"s_19\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_19\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_17)).isChecked()) {
                            data += "{\"id\": \"s_20\",\"choice_id\": \"present\"},";
                        } else {
                            data += "{\"id\": \"s_20\",\"choice_id\": \"absent\"},";
                        }
                        if (((CheckBox) findViewById(R.id.musculoskeletal_system_checkbox_18)).isChecked()) {
                            data += "{\"id\": \"s_21\",\"choice_id\": \"present\"}]";
                        } else {
                            data += "{\"id\": \"s_21\",\"choice_id\": \"absent\"}]";
                        }
                        getDiagnosis(data,sexo,ageo);
                        dialog.dismiss();
                    }
                });

            }
      });
    }
}
