package com.iwelliclinicpatient;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Wallet extends AppCompatActivity implements PurchasesUpdatedListener {
    static TextView WalletButton;
    List<String> SymptomsList;
    JSONArray finalEvidence;
    Boolean stop = false;
    String DiagnosisResult = "";
    JSONObject questionObject = null;
    TextView text;
    TextView tv_check_arrhythmia;
    TextView heading_amount;
    BillingClient billingClient;

    int languageCode;

    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }
    }

    public void languageUI(int languageCode) {
        if (languageCode == 0) {
            WalletButton.setText(getResources().getString(R.string.proceed_to_diagnosisgr));
            tv_check_arrhythmia.setText(getResources().getString(R.string.addsymptomsgr));
        } else {
            WalletButton.setText(getResources().getString(R.string.proceed_to_diagnosis));
            tv_check_arrhythmia.setText(getResources().getString(R.string.addsymptoms));
        }
    }

    private void setupBillingClient() {
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                Boolean paid = false;
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    Purchase.PurchasesResult purchasesResult = billingClient.queryPurchases(BillingClient.SkuType.SUBS);
                    List<Purchase> activeSubs = purchasesResult.getPurchasesList();
                    for (Purchase purchase : activeSubs) {
                        Toast.makeText(getApplicationContext(), purchase.getSku(), Toast.LENGTH_SHORT).show();
                        if (purchase.getSku().equals("sub_symptom_analysis")) {
                            paid = true;
                            startDiagnosis();
                            break;
                        }
                    }
                    if (!paid) {
                        List<String> skuList = new ArrayList<>();
                        skuList.add("sub_symptom_analysis");
                        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
                        billingClient.querySkuDetailsAsync(params.build(),
                                new SkuDetailsResponseListener() {
                                    @Override
                                    public void onSkuDetailsResponse(BillingResult billingResult,
                                                                     List<SkuDetails> skuDetailsList) {
                                        if (skuDetailsList.size() > 0) {
                                            BillingFlowParams flowParams = BillingFlowParams.newBuilder().setSkuDetails(skuDetailsList.get(0)).build();
                                            billingClient.launchBillingFlow(Wallet.this, flowParams);
                                        } else {
                                            Toast.makeText(getApplicationContext(), "No product in the application", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        });
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_pop_up);
        WalletButton = (TextView) findViewById(R.id.wallet_up_next_button);
        tv_check_arrhythmia = (TextView) findViewById(R.id.tv_check_arrhythmia);
        billingClient = BillingClient.newBuilder(this).setListener(this).enablePendingPurchases().build();
        SharedPreferences sharedPref = getSharedPreferences("USERDATA", MODE_PRIVATE);
        languageCode = sharedPref.getInt("language", 0);
        languageUI(languageCode);
        WalletButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (isInternetAvailable()) {
                    setupBillingClient();
                }
            }
        });
        tv_check_arrhythmia.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), symptomsActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public static void showToast(Context mContext, String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    private void getDiagnosis() {
        HttpURLConnection connection = null;
        URL url;
        try {
            url = new URL("https://api.infermedica.com/v2/diagnosis");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            connection.setRequestProperty("App-Id", "06ecefd2");
            connection.setRequestProperty("App-Key", "7f6f98ab70709a5b87111b5bc791e9ff");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            if (stop) {
                finalEvidence.remove(finalEvidence.length() - 1);
                Log.d("IN", "IN");
            }

            String dataToSend = "{\"sex\": \"" + "male" + "\",\"age\": " + "40" + ",\"evidence\":" + finalEvidence + ",\"extras\":{\"disable_groups\":true}}";
            Log.d("toSend", dataToSend);

            byte[] bytes = dataToSend.getBytes("UTF-8");
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.write(bytes);
            wr.flush();
            wr.close();
            if (connection.getResponseMessage().equals("OK")) {
                InputStream in = new BufferedInputStream(connection.getInputStream());
                ByteArrayOutputStream buf = new ByteArrayOutputStream();
                int result = in.read();
                while (result != -1) {
                    buf.write((byte) result);
                    result = in.read();
                }

                String response = buf.toString("UTF-8");
                JSONObject reader = new JSONObject(response);
                if (reader.getString("should_stop").equals("true") || stop) {
                    Log.d("IN", "IN");
                    JSONArray nameArray = reader.getJSONArray("conditions");
                    String diagnosis = "";
                    for (int i = 0; i < nameArray.length(); i++) {
                        JSONObject nameobj = nameArray.getJSONObject(i);
                        diagnosis += "Probability: " + nameobj.getString("probability") + " - " + nameobj.getString("common_name") + "\n";
                    }
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                    String finalDiagnosis = diagnosis;
                    builder.setMessage(diagnosis)
                            .setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Home.EmailDiagnosis = finalDiagnosis;
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                    android.app.AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    JSONObject question = reader.getJSONObject("question");
                    JSONArray items = question.getJSONArray("items");
                    JSONObject first = items.getJSONObject(0);
                    Home.EmailSymptoms += "," + first.getString("name");
                    questionObject = new JSONObject();
                    questionObject.put("id", first.getString("id"));

                    JSONArray nameArray = reader.getJSONArray("conditions");
                    String diagnosis = "";
                    for (int i = 0; i < nameArray.length(); i++) {
                        JSONObject nameobj = nameArray.getJSONObject(i);
                        diagnosis += "Probability: " + nameobj.getString("probability") + " - " + nameobj.getString("common_name") + "\n";
                    }

                    String QuestionToAnswer = "";

                    if (question.getString("text").equals("Have you been bleeding a lot recently?")) {
                        final int[] heartRate = new int[1];
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Wallet.this);
                        alertDialog.setTitle("Diagnosis");
                        alertDialog.setMessage("Please enter your heart rate");

                        final EditText input = new EditText(Wallet.this);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.MATCH_PARENT);
                        input.setLayoutParams(lp);
                        alertDialog.setView(input);

                        alertDialog.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        heartRate[0] = Integer.parseInt(input.getText().toString());
                                        try {
                                            if (heartRate[0] < 100) {
                                                questionObject.put("choice_id", "present");
                                                Home.EmailSymptoms += ":present";
                                            } else {
                                                questionObject.put("choice_id", "absent");
                                                Home.EmailSymptoms += ":absent";
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        finalEvidence.put(questionObject);
                                        getDiagnosis();
                                    }
                                });
                        alertDialog.show();
                    } else {
                        QuestionToAnswer = question.getString("text");

                        AlertDialog.Builder builder = new AlertDialog.Builder(Wallet.this);
                        builder.setView(LayoutInflater.from(Wallet.this).inflate(R.layout.questions_alert_design, null));
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        TextView diagnosisText = (TextView) dialog.findViewById(R.id.symp_alert_recycler_view);
                        diagnosisText.setMovementMethod(new ScrollingMovementMethod());
                        TextView questionTouser = (TextView) dialog.findViewById(R.id.symp_alert_questionToUser);
                        TextView answerYes = (TextView) dialog.findViewById(R.id.symp_alert_proceed_to_diagnosis);
                        TextView answerNo = (TextView) dialog.findViewById(R.id.symp_alert_no);
                        TextView answerIdontKnow = (TextView) dialog.findViewById(R.id.idont);
                        TextView answerFinish = (TextView) dialog.findViewById(R.id.finish);
                        TextView symp_alert_heading_symptoms = (TextView) dialog.findViewById(R.id.symp_alert_heading_symptoms);
                        TextView symp_alert_heading_current_diagnosis = (TextView) dialog.findViewById(R.id.symp_alert_heading_current_diagnosis);
                        TextView symp_alert_question = (TextView) dialog.findViewById(R.id.symp_alert_question);

                        if (languageCode == 0) {
                            answerYes.setText("Ναι");
                            answerNo.setText("Όχι");
                            answerIdontKnow.setText("Δεν ξέρω");
                            answerFinish.setText("Τέλος");
                            symp_alert_heading_symptoms.setText(getResources().getString(R.string.symptomsDiagnosisgr));
                            symp_alert_heading_current_diagnosis.setText(getResources().getString(R.string.symptomsDiagnosisgr));
                            diagnosisText.setText(getResources().getString(R.string.in_order_to_get_a_diagnosis_please_proceed_to_the_paymentgr));
                            questionTouser.setText(getResources().getString(R.string.in_order_to_get_a_diagnosis_please_proceed_to_the_paymentgr));
                            symp_alert_question.setText(getResources().getString(R.string.questiongr));
                        } else {
                            answerYes.setText("Yes");
                            answerNo.setText("No");
                            answerIdontKnow.setText("I don't know");
                            symp_alert_heading_symptoms.setText(getResources().getString(R.string.symptomsDiagnosis));
                            answerFinish.setText("Finish");
                            symp_alert_heading_current_diagnosis.setText(getResources().getString(R.string.symptomsDiagnosis));
                            diagnosisText.setText(getResources().getString(R.string.in_order_to_get_a_diagnosis_please_proceed_to_the_payment));
                            questionTouser.setText(getResources().getString(R.string.in_order_to_get_a_diagnosis_please_proceed_to_the_payment));
                            symp_alert_question.setText(getResources().getString(R.string.question));
                        }

                        diagnosisText.setText(diagnosis);
                        questionTouser.setText(QuestionToAnswer);

                        answerYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    questionObject.put("choice_id", "present");
                                    Home.EmailSymptoms += ":present";
                                    finalEvidence.put(questionObject);
                                    getDiagnosis();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        answerNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    questionObject.put("choice_id", "absent");
                                    Home.EmailSymptoms += ":absent";
                                    finalEvidence.put(questionObject);
                                    getDiagnosis();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        answerIdontKnow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    questionObject.put("choice_id", "unknown");
                                    Home.EmailSymptoms += ":unknown";
                                    finalEvidence.put(questionObject);
                                    getDiagnosis();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        answerFinish.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stop = true;
                                finalEvidence.put(questionObject);
                                getDiagnosis();
                            }
                        });
                    }
                }
                stop = false;
            } else {
                Toast.makeText(getApplicationContext(), "Something went wrong!", Toast.LENGTH_SHORT).show();
            }
        } catch (UnknownHostException | JSONException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (ProtocolException e) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (ConnectException e) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (MalformedURLException e) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    private void startDiagnosis() {
        if(isInternetAvailable()) {
            if (Home.EmailSymptoms != "") {
                SymptomsList = new ArrayList<String>(Arrays.asList(Home.EmailSymptoms.split(",")));

                for (Integer i = 0; i < SymptomsList.size(); i++) {
                    SymptomsList.set(i, SymptomsList.get(i).substring(0, SymptomsList.get(i).indexOf(' ')));
                }

                finalEvidence = new JSONArray();
                for (int i = 0; i < SymptomsList.size(); i++) {
                    try {

                        JSONObject jo = new JSONObject();
                        String id = "";
                        if (SymptomsList.get(i).substring(0, 1).equals(" ")) {
                            id = SymptomsList.get(i).substring(1);
                        } else {
                            id = SymptomsList.get(i);
                        }
                        jo.put("id", id);
                        jo.put("initial", true);
                        jo.put("choice_id", "present");
                        finalEvidence.put(jo);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Home.EmailDiagnosis = "";
                getDiagnosis();
            } else {
                showToast(getApplicationContext(), "Please connect to the Internet");
            }
        }else{
            showToast(getApplicationContext(), "Please connect to the Internet");
        }
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, List<Purchase> purchases) {
        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && purchases != null) {
            startDiagnosis();
        } else if (purchases != null) {
            startDiagnosis();
        } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
        } else if(billingResult.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED){
            startDiagnosis();
        }else{
            Toast.makeText(getApplicationContext(), "NOTHING FOUND", Toast.LENGTH_SHORT).show();
        }
    }
}
