package com.iwelliclinicpatient;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.borsam.device.BorsamDevice;
import com.borsam.pdf.ECGPdfCreator;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.util.FitPolicy;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Borsam Medical
 * @version 1.0
 **/
public class ECGPdfViewer extends AppCompatActivity {

    private static final String BORSAM_DEVICE = "borsam_device";
    private PDFView mPDFView;
    private BorsamDevice mBorsamDevice;
    private String mPdfPath;
    private AsyncTask<Void, Void, Boolean> mTask;
    static TextView DoneButton;

    public static void start(Context context, @NonNull BorsamDevice borsamDevice) {
        Intent starter = new Intent(context, ECGPdfViewer.class);
        starter.putExtra(BORSAM_DEVICE, borsamDevice);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        mPDFView = findViewById(R.id.pdfView);
        mBorsamDevice = getIntent().getParcelableExtra(BORSAM_DEVICE);
        createPdf();
        DoneButton = (TextView)findViewById(R.id.dpne);


        DoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("finish_activity");
                sendBroadcast(intent);
                finish();
            }
        });

    }

    /**
     * createPdf
     */
    @SuppressLint("StaticFieldLeak")
    private void createPdf() {
        mTask = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                File pdfParentFile = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);

              //  File pdfFile = new File(pdfParentFile, "test.pdf");
                File pdfFile = new File(getExternalFilesDir(null).getAbsolutePath(), "ecg.pdf");

                mPdfPath = pdfFile.getPath();
                ECGPdfCreator ecgPdfCreator = new ECGPdfCreator(ECGPdfViewer.this, mPdfPath);
                //BorsamDevice#getRecordData if you want to upload record data,
                //use this method to save data as file and upload it
                if(Home.ECGType!=3) {
                    ecgPdfCreator.setData(mBorsamDevice.getRecordData(), mBorsamDevice.getSampling(), mBorsamDevice.getADUnit(), mBorsamDevice.getPassageNumbers(), mBorsamDevice.isReverse());
                }else{
                    File file = new File(getExternalFilesDir(null).getAbsolutePath() +"/ECGData.txt");
                    int size = (int) file.length();
                    byte[] bytes = new byte[size];
                    try {
                        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                        buf.read(bytes, 0, bytes.length);
                        buf.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ecgPdfCreator.setData(bytes, mBorsamDevice.getSampling(), mBorsamDevice.getADUnit(), mBorsamDevice.getPassageNumbers(), mBorsamDevice.isReverse());
                }
                return ecgPdfCreator.createPdf();
            }

            @Override
            protected void onPostExecute(Boolean result) {
                if (result != null && result) {
                    mPDFView
                            .fromFile(new File(mPdfPath))
                            .pageFitPolicy(FitPolicy.BOTH)
                            .load();

                    Intent intent = new Intent("finish_activity");
                    sendBroadcast(intent);
                }
            }
        };
        mTask.execute();
        Intent intent = new Intent("finish_activity");
        sendBroadcast(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            ArrayList<String> arraySpinner = new ArrayList<String>();
            arraySpinner.clear();
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arraySpinner);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent("finish_activity");
        sendBroadcast(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mTask != null) {
            mTask.cancel(true);
        }
        //IMPORTANT BorsamDevice#close will clear all datas and listeners
        mBorsamDevice.close();
    }
}
