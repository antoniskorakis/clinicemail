package com.iwelliclinicpatient;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateMeasurement extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 124;
    private static final String TAG = "test";
    private ImageView toolbarbackgroundcolor;
    private ImageView backbutton;
    private ConstraintLayout circleimagelayout;
    private TextView toolbartext;
    private ConstraintLayout righticonslayout;

    TextView pressure1;
    TextView pressure2;
    TextView oxygen;
    TextView weight;
    TextView heartrate;
    TextView gluco;
    TextView temperature;

    TextView pressure1Text;
    TextView oxygenText;
    TextView glucoText;
    TextView weightText;
    TextView heartrateText;
    TextView button;
    TextView temperatureText;


    public static int languageCode = 0;
    ArrayList<String> values = new ArrayList<>();
    Boolean VoiceCommand = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vital_and_notes_update);

        pressure1Text = (TextView) findViewById(R.id.blood_pressure);
        oxygenText = (TextView) findViewById(R.id.blood_oxygen);
        glucoText = (TextView) findViewById(R.id.blood_glucose);
        weightText = (TextView) findViewById(R.id.weight);
        heartrateText = (TextView) findViewById(R.id.heart_rate);
        temperatureText = (TextView) findViewById(R.id.temperature);

        pressure1 = (TextView) findViewById(R.id.et_blood_pressure_text_left);
        pressure2 = (TextView) findViewById(R.id.et_blood_pressure_text_right);
        oxygen = (TextView) findViewById(R.id.et_blood_oxygen_text);
        weight = (TextView) findViewById(R.id.et_weight_text);
        heartrate = (TextView) findViewById(R.id.et_heart_rate_text);
        gluco = (TextView) findViewById(R.id.et_blood_glucose_text);
        temperature = (TextView) findViewById(R.id.et_temperature_text);
        pressure1.setText(Home.bloodPressure1);
        pressure2.setText(Home.bloodPressure2);
        oxygen.setText(Home.oxygen);
        weight.setText(Home.weight);
        heartrate.setText(Home.heartrate);
        gluco.setText(Home.gluco);
        temperature.setText(Home.temp);

        Home.EmailVital="";
        Button fab = findViewById(R.id.mic_select);

        SharedPreferences sharedPref = getSharedPreferences("USERDATA", MODE_PRIVATE);

        button = (TextView) findViewById(R.id.done_button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                values.clear();
                SendMeasurement();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startVoiceInput();
            }
        });

        languageCode = sharedPref.getInt("language", 0);
        if (languageCode == 0) {
            button.setText(getResources().getString(R.string.SendGR));
            pressure1Text.setText(getResources().getString(R.string.bloodpressureGR));
            oxygenText.setText(getResources().getString(R.string.bloodoxygenGR));
            glucoText.setText(getResources().getString(R.string.bloodglucoseGR));
            weightText.setText(getResources().getString(R.string.weightGR));
            heartrateText.setText(getResources().getString(R.string.heartrateGR));
            temperatureText.setText(getResources().getString(R.string.temperaturegr));
        } else {
            button.setText(getResources().getString(R.string.SendEN));
            pressure1Text.setText(getResources().getString(R.string.bloodpressureEN));
            oxygenText.setText(getResources().getString(R.string.bloodoxygenEN));
            glucoText.setText(getResources().getString(R.string.bloodglucoseEN));
            weightText.setText(getResources().getString(R.string.weightEN));
            heartrateText.setText(getResources().getString(R.string.heartrateEN));
            temperatureText.setText(getResources().getString(R.string.temperature));
        }
    }

    private void startVoiceInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        if(languageCode==0) {
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "ΠΕΙΤΕ ΤΟ ΕΙΔΟΣ ΤΗΣ ΜΕΤΡΗΣΗΣ ΚΑΙ ΤΗΝ ΤΙΜΗ ΤΗΣ");
        }else{
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Say the measurement with its value");
        }
        try {
            startActivityForResult(intent, 100);
        } catch (ActivityNotFoundException a) {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 100: {
                if (resultCode == RESULT_OK && null != data) {
                    values.clear();
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Pattern p = Pattern.compile("\\d+(?:\\.\\d+)?");

                    Matcher m = p.matcher(result.get(0).replace(",","."));
                    while (m.find()) {
                        values.add(m.group(0));
                    }
                    if (result.get(0).toUpperCase().contains("ΠΊΕΣΗ") || result.get(0).toUpperCase().contains("ΠΙΕΣΉ") || result.get(0).toUpperCase().contains("BLOOD PRESSURE")) {
                        if (values.size() > 1) {
                            VoiceCommand = true;
                            pressure1.setText(values.get(0));
                            pressure2.setText(values.get(1));
                        } else {
                            Toast.makeText(getApplicationContext(), "Please say the values!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } else if (result.get(0).toUpperCase().contains("ΠΑΛΜΟΣ") ||result.get(0).toUpperCase().contains("ΠΑΛΜΌΣ")||result.get(0).toUpperCase().contains("ΡΥΘΜΌΣ") ||result.get(0).toUpperCase().contains("ΡΥΘΜΌΣ")|| result.get(0).toUpperCase().contains("HEART RATE")) {
                        if (values.size() > 0) {
                            VoiceCommand = true;
                            heartrate.setText(values.get(0));
                        } else {
                            Toast.makeText(getApplicationContext(), "Please say the value!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    else if (result.get(0).toUpperCase().contains("ΘΕΡΜΟΚΡΑΣΊΑ") ||result.get(0).toUpperCase().contains("ΘΕΡΜΟΚΡΑΣΙΑ")|| result.get(0).toUpperCase().contains("TEMPERATURE")) {
                        if (values.size() > 0) {
                            VoiceCommand = true;

                            temperature.setText(values.get(0));
                        } else {
                            Toast.makeText(getApplicationContext(), "Please say the value!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    else if (result.get(0).toUpperCase().contains("ΒΆΡΟΣ") || result.get(0).toUpperCase().contains("ΒΑΡΟΣ") ||result.get(0).toUpperCase().contains("WEIGHT")) {
                        if (values.size() > 0) {
                            VoiceCommand = true;
                            weight.setText(values.get(0));
                        } else {
                            Toast.makeText(getApplicationContext(), "Please say the value!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } else if (result.get(0).toUpperCase().contains("ΓΛΥΚΌΖΗ")|| result.get(0).toUpperCase().contains("ΓΛΥΚΌΖΗ")|| result.get(0).toUpperCase().contains("GLUCOSE")) {
                        if (values.size() > 0) {
                            VoiceCommand = true;
                            gluco.setText(values.get(0));
                        } else {
                            Toast.makeText(getApplicationContext(), "Please say the value!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } else if ( result.get(0).toUpperCase().contains("ΟΞΥΓΌΝΟ")||result.get(0).toUpperCase().contains("ΟΞΥΓΟΝΟ")|| result.get(0).toUpperCase().contains("OXYGEN")) {
                        if (values.size() > 0) {
                            VoiceCommand = true;
                            oxygen.setText(values.get(0));
                        } else {
                            Toast.makeText(getApplicationContext(), "Please say the value!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } else if (result.get(0).toUpperCase().contains("OK") || result.get(0).toUpperCase().contains("OK")) {
                        SendMeasurement();
                    }
                }
                break;
            }
        }
    }


    private void setToolBar(){
        toolbarbackgroundcolor.setVisibility(View.VISIBLE);
        backbutton.setColorFilter(getResources().getColor(R.color.white));
        toolbartext.setText("Home");
        toolbartext.setTextColor(getResources().getColor(R.color.white));
    }

    private void SendMeasurement() {
            if (pressure1.getText().toString() != null && !pressure1.getText().toString().isEmpty() && pressure2.getText().toString() != null && !pressure2.getText().toString().isEmpty()) {
                if (Double.parseDouble(pressure1.getText().toString()) != 0 && Double.parseDouble(pressure2.getText().toString()) != 0) {
                    if(languageCode==0){
                        Home.EmailVital += "Πίεση αίματος: " + pressure1.getText() + "/" + pressure2.getText() + System.getProperty("line.separator");
                    }else {
                        Home.EmailVital += "Blood Pressure: " + pressure1.getText() + "/" + pressure2.getText() + System.getProperty("line.separator");
                    }
                    Home.bloodPressure1=pressure1.getText().toString();
                    Home.bloodPressure2=pressure2.getText().toString();
                    pressure1.setText("");
                    pressure2.setText("");
                    if(languageCode==0){
                        Home.VitalsButton.setText("ΜΕΤΡΗΣΕΙΣ ΥΓΕΙΑΣ");
                    }else {
                        Home.VitalsButton.setText("VIEW VITAL SIGNS");
                    }
                    Home.VitalsImage.setImageResource(R.drawable.ic_heart_tick);
                    Home.SendEmail=true;
                }
            }else{
                Home.bloodPressure1="";
                Home.bloodPressure2="";
            }


            if (oxygen.getText().toString() != null && !oxygen.getText().toString().isEmpty()) {
                if (Double.parseDouble(oxygen.getText().toString()) != 0) {
                    if(languageCode==0){
                        Home.EmailVital += "Οξυγόνο αίματος: " + oxygen.getText() + System.getProperty("line.separator");
                    }else {
                        Home.EmailVital += "Blood Oxygen: " + oxygen.getText() + System.getProperty("line.separator");
                    }
                    Home.oxygen=oxygen.getText().toString();
                    oxygen.setText("");
                    if(languageCode==0){
                        Home.VitalsButton.setText("ΜΕΤΡΗΣΕΙΣ ΥΓΕΙΑΣ");
                    }else {
                        Home.VitalsButton.setText("VIEW VITAL SIGNS");
                    }
                    Home.VitalsImage.setImageResource(R.drawable.ic_heart_tick);
                    Home.SendEmail=true;
                }
            }else{
                Home.oxygen="";
            }

            if (weight.getText().toString() != null && !weight.getText().toString().isEmpty()) {
                if (Double.parseDouble(weight.getText().toString()) != 0) {
                    if(languageCode==0){
                        Home.EmailVital += "Βάρος σώματος: " + weight.getText() + System.getProperty("line.separator");
                    }else {
                        Home.EmailVital += "Body Weight: " + weight.getText() + System.getProperty("line.separator");
                    }
                    Home.weight=weight.getText().toString();
                    weight.setText("");
                    if(languageCode==0){
                        Home.VitalsButton.setText("ΜΕΤΡΗΣΕΙΣ ΥΓΕΙΑΣ");
                    }else {
                        Home.VitalsButton.setText("VIEW VITAL SIGNS");
                    }
                    Home.VitalsImage.setImageResource(R.drawable.ic_heart_tick);
                    Home.SendEmail=true;
                }
            }else{
                Home.weight="";
            }

        if (temperature.getText().toString() != null && !temperature.getText().toString().isEmpty()) {
            if (Double.parseDouble(temperature.getText().toString()) != 0) {
                if(languageCode==0){
                    Home.EmailVital += "Θερμοκρασία σώματος: " + temperature.getText() + System.getProperty("line.separator");
                }else {
                    Home.EmailVital += "Body Temperature: " + temperature.getText() + System.getProperty("line.separator");
                }
                Home.temp=temperature.getText().toString();
                temperature.setText("");
                if(languageCode==0){
                    Home.VitalsButton.setText("ΜΕΤΡΗΣΕΙΣ ΥΓΕΙΑΣ");
                }else {
                    Home.VitalsButton.setText("VIEW VITAL SIGNS");
                }
                Home.VitalsImage.setImageResource(R.drawable.ic_heart_tick);
                Home.SendEmail=true;
            }
        }else{
            Home.temp="";
        }


            if (heartrate.getText().toString() != null && !heartrate.getText().toString().isEmpty()) {
                if (Double.parseDouble(heartrate.getText().toString()) != 0) {
                    if(languageCode==0){
                        Home.EmailVital += "Καρδιακός ρυθμός: " + heartrate.getText() + System.getProperty("line.separator");
                    }else {
                        Home.EmailVital += "Heart Rate: " + heartrate.getText() + System.getProperty("line.separator");
                    }
                    Home.heartrate=heartrate.getText().toString();
                    heartrate.setText("");
                    if(languageCode==0){
                        Home.VitalsButton.setText("ΜΕΤΡΗΣΕΙΣ ΥΓΕΙΑΣ");
                    }else {
                        Home.VitalsButton.setText("VIEW VITAL SIGNS");
                    }
                    Home.VitalsImage.setImageResource(R.drawable.ic_heart_tick);
                    Home.SendEmail=true;
                }
            }else{
                Home.heartrate="";
            }

            if (gluco.getText().toString() != null && !gluco.getText().toString().isEmpty()) {
                if (Double.parseDouble(gluco.getText().toString()) != 0) {
                    if(languageCode==0){
                        Home.EmailVital += "Γλυκόζη αίματος: " + gluco.getText() + System.getProperty("line.separator");
                    }else {
                        Home.EmailVital += "Blood Glucose: " + gluco.getText() + System.getProperty("line.separator");
                    }
                    Home.gluco=gluco.getText().toString();
                    gluco.setText("");
                    if(languageCode==0){
                        Home.VitalsButton.setText("ΜΕΤΡΗΣΕΙΣ ΥΓΕΙΑΣ");
                    }else {
                        Home.VitalsButton.setText("VIEW VITAL SIGNS");
                    }
                    Home.VitalsImage.setImageResource(R.drawable.ic_heart_tick);
                    Home.SendEmail=true;
                }
            }else{
                Home.gluco="";
            }
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}

