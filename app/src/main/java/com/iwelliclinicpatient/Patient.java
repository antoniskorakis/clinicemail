package com.iwelliclinicpatient;

import java.util.ArrayList;

public class Patient {
    private String pID;
    private String pName;

    public Patient(String pid, String pname) {
        pID = pid;
        pName = pname;
    }

    public String getID() {
        return pID;
    }

    public String getName() {
        return pName;
    }

    private static int lastPatientId = 0;

    public static ArrayList<Patient> createPatientsList(int numContacts) {
        ArrayList<Patient> contacts = new ArrayList<Patient>();

        for (int i = 1; i <= numContacts; i++) {
            contacts.add(new Patient("ID", "name"));
        }
        return contacts;
    }
}
