package com.iwelliclinicpatient;

import androidx.multidex.MultiDexApplication;

import com.borsam.ble.BorsamBleManager;

public class App extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        BorsamBleManager.getInstance().init(this);
    }
}
