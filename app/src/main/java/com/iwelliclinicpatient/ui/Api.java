package com.iwelliclinicpatient.ui;

import java.util.Map;

import com.iwelliclinicpatient.bean.BorsamResult;
import com.iwelliclinicpatient.bean.Config;
import com.iwelliclinicpatient.bean.LoginResult;
import com.iwelliclinicpatient.bean.UploadResult;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * @author GreenhairTurtle
 * <br> email:wuyongjie2008@gmail.com
 * @version 1.0
 * @date 2019/1/22  17:30
 * @description retrofit
 **/
public interface Api {
  @GET
  Call<BorsamResult<Config>> getConfig(@Url String url, @QueryMap Map<String,String> params);

  /**
   * get方法 用于不在乎返回内容的请求
   * http get,used for requests that don't care about returning content
   */
  @GET
  Call<BorsamResult> get(@Url String url, @QueryMap Map<String, String> params);

  /**
   * post方法 用于不在乎返回内容的请求
   * http post,used for requests that don't care about returning content
   */
  @POST
  Call<BorsamResult> post(@Url String url, @Body Map<String, String> params);

  /**
   * 登陆
   */
  @POST
  Call<BorsamResult<LoginResult>> login(@Url String url, @Body Map<String, String> params);

  /*
   * 上传文件
   */
  @POST
  Call<BorsamResult<UploadResult>> uploadFile(@Url String url, @Body RequestBody file);
}
