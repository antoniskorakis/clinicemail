package com.iwelliclinicpatient;

import java.util.ArrayList;
import java.util.Iterator;

public class Category {

    public ArrayList<Category> children;
    public ArrayList<String> selection;


    public String name;

    public Category() {
        children = new ArrayList<Category>();
        selection = new ArrayList<String>();
    }

    public Category(String name) {
        this();
        this.name = name;
    }

    public String toString() {
        return this.name;
    }

    // generate some random amount of child objects (1..10)
    private void generateChildren(int category, int languageCode) {
        if (languageCode == 0) {
            switch (category) {
                case 1:
                    this.children.add(new Category("s_116 ΑΙΜΑ ΣΤΑ ΦΛΕΓΜΑΤΑ Η ΣΤΟ ΣΑΛΙΟ (ΑΙΜΟΠΤΥΣΗ)"));
                    this.children.add(new Category("s_1429 ΑΙΜΟΡΡΑΓΙΑ ΑΠΟ ΤΗ ΜΥΤΗ (ΡΙΝΟΡΡΑΓΙΑ)"));
                    this.children.add(new Category("s_407 ΒΟΥΙΣΜΑ ΣΤΑ ΑΥΤΙΑ (ΕΜΒΟΕΣ)"));
                    this.children.add(new Category("s_207 ΔΙΠΛΩΠΙΑ"));
                    this.children.add(new Category("s_936 ΖΑΛΑΔΑ (ΖΑΛΗ)"));
                    this.children.add(new Category("s_500 ΘΟΛΗ ΟΡΑΣΗ (ΘΑΜΒΟΣ ΟΡΑΣΕΩΣ)"));
                    this.children.add(new Category("s_21 ΠΟΝΟΚΕΦΑΛΟΣ (ΚΕΦΑΛΑΛΓΙΑ)"));
                    this.children.add(new Category("s_616 ΠΡΗΞΙΜΟ ΤΟΥ ΠΡΟΣΩΠΟΥ (ΟΙΔΗΜΑ ΠΡΟΣΩΠΟΥ)"));
                    break;
                case 2:
                    this.children.add(new Category("s_102 ΒΗΧΑΣ"));
                    this.children.add(new Category("s_88 ΔΥΣΠΝΟΙΑ"));
                    this.children.add(new Category("s_1333 ΛΑΧΑΝΙΑΣΜΑ"));
                    this.children.add(new Category("s_50 ΠΟΝΟΣ ΣΤΟ ΘΩΡΑΚΑ"));
                    this.children.add(new Category("s_50 ΠΟΝΟΣ ΣΤΟ ΣΤΗΘΟΣ"));
                    break;
                case 3:
                    this.children.add(new Category("s_153 ΑΚΡΑΤΕΙΑ ΟΥΡΩΝ"));
                    this.children.add(new Category("s_8 ΔΙΑΡΡΟΙΑ"));
                    this.children.add(new Category("s_329 ΔΥΣΚΟΙΛΙΟΤΗΤΑ"));
                    this.children.add(new Category("s_305 ΕΜΕΤΟΣ"));
                    this.children.add(new Category("s_309 ΦΟΥΣΚΩΜΑ ΣΤΗΝ ΚΟΙΛΙΑ"));
                    break;
                case 4:
                    this.children.add(new Category("s_586 ΓΥΝΑΙΚΟΜΑΣΤΙΑ"));
                    break;
                case 5:
                    this.children.add(new Category("s_120 ΑΓΧΟΣ"));
                    this.children.add(new Category("s_1345 ΣΥΓΧΥΣΗ"));
                    break;
                case 6:
                    this.children.add(new Category("s_650 ΑΔΥΝΑΜΙΑ"));
                    this.children.add(new Category("s_1624 ΑΚΙΝΗΣΙΑ"));
                    this.children.add(new Category("s_1006 ΑΛΛΟ"));
                    this.children.add(new Category("s_285 ΑΠΩΛΕΙΑ ΒΑΡΟΥΣ"));
                    this.children.add(new Category("s_284 ΑΠΩΛΕΙΑ ΟΡΕΞΗΣ"));
                    this.children.add(new Category("s_216 ΈΝΤΟΝΟΣ ΙΔΡΩΤΑΣ (ΕΦΙΔΡΩΣΗ)"));
                    this.children.add(new Category("s_188 ΛΙΠΟΘΥΜΙΑ (ΑΠΩΛΕΙΑ ΑΙΣΘΗΣΕΩΝ)"));
                    this.children.add(new Category("s_156 ΝΑΥΤΙΑ"));
                    this.children.add(new Category("s_175 ΟΙΔΗΜΑ ΚΑΤΩ ΑΚΡΩΝ"));
                    this.children.add(new Category("s_1835 ΠΟΝΟΣ (ΆΛΓΟΣ)"));
                    this.children.add(new Category("s_98 ΠΥΡΕΤΟΣ"));
                    break;
                case 7:
                    this.children.add(new Category("s_227 ΓΡΗΓΟΡΗ ΑΝΑΠΝΟΗ (ΤΑΧΥΠΝΟΙΑ)"));
                    this.children.add(new Category("s_1462 ΔΕΝ ΜΠΟΡΩ ΝΑ ΠΑΡΩ ΒΑΘΙΕΣ ΑΝΑΠΝΟΕΣ"));
                    this.children.add(new Category("s_474 ΣΦΥΡΙΓΜΑ ΚΑΤΑ ΤΗΝ ΑΝΑΠΝΟΗ (ΣΥΡΙΓΜΟΣ)"));
                    break;
                case 8:
                    this.children.add(new Category("s_534 ΒΡΑΔΥΚΑΡΔΙΕΣ"));
                    this.children.add(new Category("s_261 ΤΑΧΥΠΑΛΜΙΑ"));
                    this.children.add(new Category("s_110 ΓΡΗΓΟΡΟΙ Η ΑΚΑΝΟΝΙΣΤΟΙ ΧΤΥΠΟΙ ΚΑΡΔΙΑΣ"));
                    this.children.add(new Category("s_543 ΥΠΕΡΤΑΣΗ(ΥΨΗΛΗ ΠΙΕΣΗ)"));
                    this.children.add(new Category("s_533 ΥΠΟΤΑΣΗ(ΧΑΜΗΛΗ ΠΙΕΣΗ)"));
                    break;
                case 9:
                    this.children.add(new Category("s_1084 ΠΑΡΑΜΟΡΦΩΣΗ ΣΤΑ ΔΑΧΤΥΛΑ ΤΩΝ ΠΟΔΙΩΝ"));
                    this.children.add(new Category("s_175 ΠΡΗΣΜΕΝΑ ΠΟΔΙΑ (ΟΙΔΗΜΑΤΑ ΚΑΤΩ ΑΚΡΩΝ)"));
                    break;
                case 10:
                    this.children.add(new Category("s_317 ΔΥΣΚΟΛΙΑ ΣΤΟ ΒΑΔΙΣΜΑ"));
                    this.children.add(new Category("s_973 ΜΟΥΔΙΑΣΜΑ ΣΤΑ ΑΝΩ ΑΚΡΑ"));
                    this.children.add(new Category("s_1343 ΜΟΥΔΙΑΣΜΑ ΣΤΑ ΚΑΤΩ ΑΚΡΑ"));
                    this.children.add(new Category("s_650 ΜΥΪΚΗ ΑΔΥΝΑΜΙΑ"));
                    this.children.add(new Category("s_1142 ΠΟΝΟΣ ΣΤΟΥΣ ΜΥΕΣ (ΜΥΑΛΓΙΑ)"));
                    break;
            }
        } else {
            switch (category) {
                case 1:
                    this.children.add(new Category("s_116 BLOOD IN THE PHLEGM OR IN THE SALIVA (HAEMOPTYSIS)"));
                    this.children.add(new Category("s_1429 NOSE BLEED"));
                    this.children.add(new Category("s_407 BUZZING IN EARS"));
                    this.children.add(new Category("s_207 DOUBLE VISION"));
                    this.children.add(new Category("s_936 DIZZINESS"));
                    this.children.add(new Category("s_500 BLURRY VISION"));
                    this.children.add(new Category("s_21 HEADACHE"));
                    this.children.add(new Category("s_616 FACE SWELLING"));
                    break;
                case 2:
                    this.children.add(new Category("s_102 COUGH"));
                    this.children.add(new Category("s_88 DYSPNEA"));
                    this.children.add(new Category("s_1333 SHORTNESS OF BREATH"));
                    this.children.add(new Category("s_50 RIBCAGE PAIN"));
                    this.children.add(new Category("s_50 CHEST PAIN"));
                    break;
                case 3:
                    this.children.add(new Category("s_153 URINARY INCONTINENCE"));
                    this.children.add(new Category("s_8 DIARRHEA"));
                    this.children.add(new Category("s_329 CONSTIPATION"));
                    this.children.add(new Category("s_305 VOMIT"));
                    this.children.add(new Category("s_309 ABDOMINAL BLOATING"));
                    break;
                case 4:
                    this.children.add(new Category("s_586 GYNECOMASTIA"));
                    break;
                case 5:
                    this.children.add(new Category("s_120 ANXIETY"));
                    this.children.add(new Category("s_1345 CONFUSION"));
                    break;
                case 6:
                    this.children.add(new Category("s_650 WEAKNESS"));
                    this.children.add(new Category("s_1624 IMMOBILITY"));
                    this.children.add(new Category("s_1006 OTHER"));
                    this.children.add(new Category("s_285 WEIGHT LOSS"));
                    this.children.add(new Category("s_284 APPETITE LOSS"));
                    this.children.add(new Category("s_216 INTENSE SWEATING (PERSPIRATION)"));
                    this.children.add(new Category("s_188 FAINTING (LOSS OF  CONSCIOUSNESS)"));
                    this.children.add(new Category("s_156 NAUSEA (MOTION SICKNESS)"));
                    this.children.add(new Category("s_175 LOWER LIMBS SWELLING"));
                    this.children.add(new Category("s_1835 PAIN (ACHE)"));
                    this.children.add(new Category("s_98 FEVER"));
                    break;
                case 7:
                    this.children.add(new Category("s_227 RAPID BREATH"));
                    this.children.add(new Category("s_1462 I CANNOT TAKE DEEP BREATH"));
                    this.children.add(new Category("s_474 BREATHING WITH DIFFICULT (WHEEZING)"));
                    break;
                case 8:
                    this.children.add(new Category("s_534 BRADYCARDIA"));
                    this.children.add(new Category("s_261 TACHYCARDIA"));
                    this.children.add(new Category("s_110 QUICK OR IRREGULAR HEART BEATS"));
                    this.children.add(new Category("s_543 HYPERTENSION (HIGH BLOOD PRESSURE)"));
                    this.children.add(new Category("s_533 HYPOTENSION (LOW BLOOD PRESSURE)"));
                    break;
                case 9:
                    this.children.add(new Category("s_1084 DISTORTION IN TOES"));
                    this.children.add(new Category("s_175 SWOLLEN FOOT (LOWER LIMBS SWELLING)"));
                    break;
                case 10:
                    this.children.add(new Category("s_317 DIFFICULTY WALKING"));
                    this.children.add(new Category("s_973 UPPER LIMBS NUMBNESS"));
                    this.children.add(new Category("s_1343 LOWER LIMBS NUMBNESS"));
                    this.children.add(new Category("s_650 MUSCULAR WEAKNESS"));
                    this.children.add(new Category("s_1142 MUSCLES PAIN (MYALGIA)"));
                    break;
            }
        }
    }

    public static ArrayList<Category> getCategories(int languageCode) {
        ArrayList<Category> categories = new ArrayList<Category>();
        Category cat1;
        if (languageCode == 0) {
            cat1 = new Category("ΚΕΦΑΛΙ - ΚΡΑΝΙΟ");
        } else {
            cat1 = new Category("HEAD - SKULL");
        }
        cat1.generateChildren(1, languageCode);
        categories.add(cat1);

        Category cat2;
        if (languageCode == 0) {
            cat2 = new Category("ΘΩΡΑΚΑΣ");
        } else {
            cat2 = new Category("CHEST");
        }
        cat2.generateChildren(2, languageCode);
        categories.add(cat2);

        Category cat3;
        if (languageCode == 0) {
            cat3 = new Category("ΚΟΙΛΙΑΣ");
        } else {
            cat3 = new Category("BELLY");
        }
        cat3.generateChildren(3, languageCode);
        categories.add(cat3);

        Category cat4;
        if (languageCode == 0) {
            cat4 = new Category("ΜΑΣΤΩΝ");
        } else {
            cat4 = new Category("BREAST");
        }
        cat4.generateChildren(4, languageCode);
        categories.add(cat4);

        Category cat5;
        if (languageCode == 0) {
            cat5 = new Category("ΨΥΧΟΛΟΓΙΚΟ");
        } else {
            cat5 = new Category("PSYCHOLOGICAL");
        }
        cat5.generateChildren(5, languageCode);
        categories.add(cat5);

        Category cat6;
        if (languageCode == 0) {
            cat6 = new Category("ΓΕΝΙΚΟ");
        } else {
            cat6 = new Category("GENERAL");
        }
        cat6.generateChildren(6, languageCode);
        categories.add(cat6);

        Category cat7;
        if (languageCode == 0) {
            cat7 = new Category("ΑΝΑΠΝΕΥΣΤΙΚΟ");
        } else {
            cat7 = new Category("RESPIRATORY");
        }
        cat7.generateChildren(7, languageCode);
        categories.add(cat7);

        Category cat8;
        if (languageCode == 0) {
            cat8 = new Category("ΚΑΡΔΙΑΓΓΕΙΑΚΟ");
        } else {
            cat8 = new Category("CARDIOVASCULAR");
        }
        cat8.generateChildren(8, languageCode);
        categories.add(cat8);

        Category cat9;
        if (languageCode == 0) {
            cat9 = new Category("ΚΑΤΩ ΑΚΡΑ");
        } else {
            cat9 = new Category("LOWER LIMBS");
        }
        cat9.generateChildren(9, languageCode);
        categories.add(cat9);

        Category cat10;
        if (languageCode == 0) {
            cat10 = new Category("ΜΥΟΣΚΕΛΕΤΙΚΟ");
        } else {
            cat10 = new Category("MUSCULOSKELETAL SYSTEM");
        }
        cat10.generateChildren(10, languageCode);
        categories.add(cat10);

        return categories;
    }

    public static Category get(String name, int languageCode) {
        ArrayList<Category> collection = Category.getCategories(languageCode);
        for (Iterator<Category> iterator = collection.iterator(); iterator.hasNext(); ) {
            Category cat = (Category) iterator.next();
            if (cat.name.equals(name)) {
                return cat;
            }

        }
        return null;
    }
}
