package com.iwelliclinicpatient;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ECGIschemia extends AppCompatActivity {
    TextView continueButton;
    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecg_ischemia);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        continueButton=(TextView) findViewById(R.id.tv_body_continue_button);
        image = (ImageView) findViewById(R.id.imageView2);
        switch(Home.IschemiaScreenNumber){
            case 1:
                image.setImageResource(R.drawable.screen1);
                break;
            case 2:
                image.setImageResource(R.drawable.screen2);
                break;
            case 3:
                image.setImageResource(R.drawable.screen3);
                break;
            case 4:
                image.setImageResource(R.drawable.screen4);
                break;
            case 5:
                image.setImageResource(R.drawable.screen5);
                break;
        }

        continueButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent2 = new Intent(ECGIschemia.this, MainActivity.class);
                startActivity(intent2);
                finish();
            }
        });
    }

}
