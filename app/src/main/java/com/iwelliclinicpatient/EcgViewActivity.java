package com.iwelliclinicpatient;

import android.Manifest;
import android.bluetooth.BluetoothGatt;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.borsam.ble.callback.BorsamBleGattCallback;
import com.borsam.ble.callback.SimpleBorsamBleDataCallback;
import com.borsam.blecore.exception.BleException;
import com.borsam.device.BorsamDevice;
import com.borsam.device.callback.DataPartTwoListener;
import com.borsam.device.callback.OnAddPointListener;
import com.borsam.network.BorsamHttp;
import com.borsam.network.BorsamRequest;
import com.borsam.widget.ECGView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.iwelliclinicpatient.bean.BorsamResult;
import com.iwelliclinicpatient.util.HttpUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Borsam Medical
 * @version 1.0
 **/
public class EcgViewActivity extends AppCompatActivity {

    //We only receive 30s data
    private float TOTAL_TIME = Home.ECGTime;
    private static final String BORSAM_DEVICE = "borsam_device";
    private BorsamDevice mBorsamDevice;
    private ECGView mECGView;
    private boolean goPdf;
    private boolean mEnd;
    int languageCode;
    TextView header;

    public static void start(Context context, @NonNull BorsamDevice borsamDevice) {
        Intent starter = new Intent(context, EcgViewActivity.class);
        starter.putExtra(BORSAM_DEVICE, borsamDevice);
        context.startActivity(starter);
    }

    private void editAccount() {
        Map<String, String> params = new HashMap<>();
        params.put("language_id", "1002");
        BorsamRequest request = BorsamHttp.getSignedBorsamRequest(HttpUtil.config.getPat_modify(), params);
        HttpUtil.getApi().post(request.getUrl(), request.getParams())
                .enqueue(new Callback<BorsamResult>() {
                    @Override
                    public void onResponse(Call<BorsamResult> call, Response<BorsamResult> response) {
                        if (response.body().isSuccessful()) {
                            Toast.makeText(EcgViewActivity.this, "User Edited!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<BorsamResult> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Home.ECGAnalysis=false;
        Home.ECGExists=false;
        setContentView(R.layout.activity_ecgview);

        SharedPreferences sharedPref = getSharedPreferences("USERDATA", MODE_PRIVATE);
        languageCode = sharedPref.getInt("language", 0);
            /*  Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        */

        header =findViewById(R.id.data_reception_heading);
        mBorsamDevice = getIntent().getParcelableExtra(BORSAM_DEVICE);
        mECGView = findViewById(R.id.ecgView);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            }
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //IMPORTANT:set ecg params
        mECGView.setECGParams(mBorsamDevice);
        mECGView.setLineColor(Color.BLACK);
        mECGView.setDotColor(Color.BLACK);
        //IMPORTANT:set this listener to display data
        mBorsamDevice.setOnAddPointListener(new OnAddPointListener() {
            /**
             * Callback this method on add point
             * @param point ecg data
             * @param passageIndex passage number start with 1 not 0
             */
            @Override
            public void onAddPoint(int point, int passageIndex) {
                mECGView.addPoint(point, passageIndex);
            }
        });
        connect();
    }

    /**
     * connect Borsam Device
     */
    private void connect() {
        //BorsamDevice#connect will clear the previously connected data
        //if you don't want,just use BorsamDevice#connect(BorsamBleGattCallback callback,boolean clearData)
        //param:clearData if <code>true</code> will clear the previously connected data
        //<code>false</code> otherwise
        mBorsamDevice.connect(new BorsamBleGattCallback() {
            /**
             * Callback in the main thread
             * Callback this method when the connection starts
             */
            @Override
            public void onStartConnect() {
                if(languageCode==0){
                    setSubtitle("Έναρξη καρδιογραφήματος");
                }else {
                    setSubtitle(R.string.start_connect);
                }
            }

            /**
             * Callback in the main thread
             * Callback method when connection fails
             */
            @Override
            public void onConnectFail(BorsamDevice borsamDevice, BleException e) {
                setSubtitle(R.string.connect_fail);
            }

            /**
             * Callback in the main thread
             * Callback this method when the connection is successful
             */
            @Override
            public void onConnectSuccess(BorsamDevice borsamDevice, BluetoothGatt bluetoothGatt,
                                         int status) {
                if(languageCode==0){
                    setSubtitle("Επιτυχής σύνδεση");
                }else {
                    setSubtitle(R.string.connect_success);
                }
                getEcgData();
            }

            /**
             * Callback in the main thread
             * Callback this method when the connection is broken
             * @param isActiveDisConnected if <code>true</code> indicate disconnect by {@link BorsamDevice#disConnect()}
             * <code>false</code> disconnect by device
             */
            @Override
            public void onDisConnected(boolean isActiveDisConnected, BorsamDevice borsamDevice,
                                       BluetoothGatt bluetoothGatt, int status) {
                setSubtitle(R.string.disconnect);
                Intent intent = new Intent("finish_activity");
                sendBroadcast(intent);
                Intent intent2 = new Intent(EcgViewActivity.this, MainActivity.class);
                startActivity(intent2);
                finish();
            }
        });
    }

    /**
     * get ecg data
     * Usually, we recommend to delay the acquisition of ECG data after a successful connection.
     * But here, it’s not so particular.
     */
    private void getEcgData() {
        mBorsamDevice.getData(new SimpleBorsamBleDataCallback() {
            @Override
            public void onDataSuccess() {
                if(languageCode==0){
                    setSubtitle("Λήψη δεδομένων");
                }else {
                    setSubtitle(R.string.getdata_success);
                }
            }

            private void writeToFile(byte[] data) {
                FileOutputStream outputStream;
                try {
                    File file = new File(getExternalFilesDir(null).getAbsolutePath() + "/ECGData.txt");
                    if(Home.ECGType!=3) {
                        outputStream = new FileOutputStream(file);
                    }else{
                        outputStream = new FileOutputStream(file,true);
                    }
                    outputStream.write(data);
                    outputStream.close();
                    Home.ECGExists=true;
                    Home.SendEmail=true;
                    if(languageCode==0){
                        Home.ECGButton.setText("ΑΝΑΛΥΣΗ ΗΛΚ");
                    }
                    else {
                        Home.ECGButton.setText("GET ECG ANALYSIS");
                    }
                    Home.ECGImage.setImageResource(R.drawable.ic_ecg_tick);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            /**
             * data failure
             */
            @Override
            public void onDataFailure(BleException e) {
                setSubtitle(R.string.getdata_failure);
            }

            /**
             * data progress
             * @param second unit:s
             */
            @Override
            public void onDataProgress(float second) {
                if (mEnd){
                    return;
                }

                switch (Home.ECGType)
                {
                    case 1:
                        if(languageCode==0){
                            setSubtitle(getString(R.string.getdata_progressgr, second) + "/30.00s");
                        }else {
                            setSubtitle(getString(R.string.getdata_progress, second) + "/30.00s");
                        }
                        break;
                    case 3:
                        if(languageCode==0){
                            setSubtitle(getString(R.string.getdata_progressgr, second) + "/15.00s");
                        }else {
                            setSubtitle(getString(R.string.getdata_progress, second) + "/15.00s");
                        }
                        break;
                    case 5:
                        if(languageCode==0){
                            setSubtitle(getString(R.string.getdata_progressgr, second) + "/120.00s");
                        }else {
                            setSubtitle(getString(R.string.getdata_progress, second) + "/120.00s");
                        }
                        break;
                }

                if (second >= TOTAL_TIME) {
                    writeToFile(mBorsamDevice.getRecordData());
                    getDataEnd();
               }
            }
        });
    }

    /**
     * get data end
     * Some devices have the second part of the data
     * So we need to judge here
     */
    private void getDataEnd() {
        mEnd = true;
        if (mBorsamDevice.hasDataPartTwo()) {
            mBorsamDevice.setDataPartTwoListener(new DataPartTwoListener() {
                @Override
                public void onDataStart() {
                    setSubtitle(R.string.getdata_second_start);
                }

                /**
                 * Second part progress
                 * @param progress from 0~100
                 */
                @Override
                public void onDataProgress(float progress) {
                    if(languageCode==0){
                        setSubtitle(getString(R.string.getdata_progressgr, progress));
                    }else {
                        setSubtitle(getString(R.string.getdata_second_progress, progress));
                    }
                }

                @Override
                public void onDataEnd(boolean success) {
                    if (success) {
                        setSubtitle(getString(R.string.getdata_second_success));
                        goPdf();
                    } else {
                        setSubtitle(getString(R.string.getData_second_failure));
                    }
                }
            });
        } else {

            if(Home.ECGType!=3) {
                goPdf();
            }else{
                if(Home.IschemiaScreenNumber>4) {
                    goPdf();
                }else{
                    Home.IschemiaScreenNumber++;
                    Intent intent = new Intent("finish_activity");
                    sendBroadcast(intent);
                    Intent intent2 = new Intent(EcgViewActivity.this, ECGIschemia.class);
                    startActivity(intent2);
                    finish();}
            }
        }
    }

    /**
     * Jump Pdf Page
     */
    private void goPdf() {
        goPdf = true;
        ECGPdfViewer.start(this,mBorsamDevice);
        finish();
    }

    private void setSubtitle(CharSequence title) {
        header.setText(title);
        /*
        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(title);
        }
         */
    }

    private void setSubtitle(@StringRes int titleRes) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(titleRes);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent("finish_activity");
        sendBroadcast(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //BorsamDevice#close will disconnect device and clear all datas and listeners
        //if you don't want to clear datas,use BorsamDevice#disconnect
        if (goPdf) {
            mBorsamDevice.disConnect();
        } else {
            mBorsamDevice.close();
        }
    }
}
